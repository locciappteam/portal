using Portal.Core.Data;
using Portal.Core.Entidades.LojasVarejo;
using Portal.Core.Entidades.Universal;
using Portal.Core.Repositorios.LojasVarejo;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Xml;
using System.Xml.XPath;

namespace Portal.App
{
    class Program
    {
        private static ViewsDataContext _views = new ViewsDataContext();
        private static irptControlShopToken _rptControlShopToken = new rptControlShopToken();
        private static irptControlShopCtrl _rptControlShopCtrl = new rptControlShopCtrl();
        
        static void Main(string[] args)
        {
            CorporateShopWS();

            Console.ReadKey();
        }

        static void CorporateShopWS()
        {
            IEnumerable<LJ_CONTROLSHOP_TOKEN> Tokens = _rptControlShopToken.Obter();
            string CaminhoXML = @"C:\Portal Loccitane 2.0\Portal\Portal.UI\Uploads\ControlShop\XMLs\";

            DateTime DataInicial = DateTime.Now.AddDays(-1);
            DateTime DataFinal = DateTime.Now.AddDays(-1);

            foreach (var Token in Tokens)
            {
                if (!Directory.Exists(CaminhoXML))
                {
                    Directory.CreateDirectory(CaminhoXML);
                }
                else
                {
                    Directory.Delete(CaminhoXML, true);
                    Directory.CreateDirectory(CaminhoXML);
                }

                //Console.WriteLine("Enviando XML: " + Token.ESTABELECIMENTO.DESC_ESTAB);

                ICollection<W_CONTROLSHOP_XML> XMLs = (
                        from
                            a in _views.W_CONTROLSHOP_XMLs
                        where
                            a.ESTABELECIMENTOID == Token.ESTABELECIMENTOID
                            && a.DATA_VENDA >= DataInicial
                            && a.DATA_VENDA <= DataFinal
                        select
                            a
                    ).ToList();

                if (XMLs.Any())
                {
                    XmlDocument docXML = new XmlDocument();
                    foreach (var XML in XMLs)
                    {
                        docXML.LoadXml(XML.ARQUIVO_XML.ToString());

                        var ChaveNFe = docXML.SelectSingleNode("//@Id").Value;

                        docXML.Save(CaminhoXML + ChaveNFe + ".xml");
                    }

                    string ArquivoZip = CaminhoXML.Replace("XMLs\\", "") + Token.ESTABELECIMENTO.CNPJ + "_" + DateTime.Today.ToString("yyyyMMdd") + ".zip";

                    if (!System.IO.File.Exists(ArquivoZip))
                    {
                        ZipFile.CreateFromDirectory(
                            CaminhoXML,
                            ArquivoZip
                        );
                    }
                    else
                    {
                        System.IO.File.Delete(ArquivoZip);

                        ZipFile.CreateFromDirectory(
                            CaminhoXML,
                            ArquivoZip
                        );
                    }

                    byte[] ZipByte = System.IO.File.ReadAllBytes(ArquivoZip);
                    string TokenWebService = Token.TOKEN_WEBSERVICE;
                    string NomeDoArquivo = Path.GetFileName(ArquivoZip);

                    var ws = new wsCorporateShop.CorporateSoapClient();
                    string Retorno = ws.RecebePacoteNotas(TokenWebService, NomeDoArquivo, ZipByte);

                    LJ_CONTROLSHOP_CONTROLE _LjControleShopControle = new LJ_CONTROLSHOP_CONTROLE
                    {
                        ESTABELECIMENTOID = Token.ESTABELECIMENTOID,
                        DATA_INICIO = DataInicial,
                        DATA_FINAL = DataFinal,
                        QTDE_XML = XMLs.Count(),
                        CADASTRADO_POR = "INTEGRACAO",
                        DATA_CADASTRO = DateTime.Now,
                        ALTERADO_POR = "INTEGRACAO",
                        DATA_ALTERACAO = DateTime.Now,
                        LJ_CONTROLSHOP_RETORNOs = new List<LJ_CONTROLSHOP_RETORNO>()
                    };
                    LJ_CONTROLSHOP_RETORNO _LjControlShopRetorno = new LJ_CONTROLSHOP_RETORNO
                    {
                        CADASTRADO_POR = "INTEGRACAO",
                        DATA_CADASTRO = DateTime.Now,
                        ALTERADO_POR = "INTEGRACAO",
                        DATA_ALTERACAO = DateTime.Now
                    };

                    if (Retorno.Contains("sucesso"))
                    {
                        if (!System.IO.File.Exists(@"C:\Portal Loccitane 2.0\Portal\Portal.UI\Uploads\ControlShop\Packages\" + Path.GetFileName(ArquivoZip)))
                        {
                            System.IO.File.Move(ArquivoZip, @"C:\Portal Loccitane 2.0\Portal\Portal.UI\Uploads\ControlShop\Packages\" + Path.GetFileName(ArquivoZip));
                        }
                        else
                        {
                            System.IO.File.Delete(@"C:\Portal Loccitane 2.0\Portal\Portal.UI\Uploads\ControlShop\Packages\" + Path.GetFileName(ArquivoZip));
                            System.IO.File.Move(ArquivoZip, @"C:\Portal Loccitane 2.0\Portal\Portal.UI\Uploads\ControlShop\Packages\" + Path.GetFileName(ArquivoZip));
                        }

                        _LjControleShopControle.ARQUIVO = "/Uploads/ControlShop/Packages/" + Path.GetFileName(ArquivoZip);
                        _LjControlShopRetorno.STATUS_RETORNO = "Arquivo recebido e descompactado com sucesso !";
                        _LjControleShopControle.LJ_CONTROLSHOP_RETORNOs.Add(_LjControlShopRetorno);

                        _rptControlShopCtrl.Adicionar(_LjControleShopControle);
                    }
                    else
                    {
                        if (!System.IO.File.Exists(@"C:\Portal Loccitane 2.0\Portal\Portal.UI\Uploads\ControlShop\Error\" + Path.GetFileName(ArquivoZip)))
                        {
                            System.IO.File.Move(ArquivoZip, @"C:\Portal Loccitane 2.0\Portal\Portal.UI\Uploads\ControlShop\Error\" + Path.GetFileName(ArquivoZip));
                        }
                        else
                        {
                            System.IO.File.Delete(@"C:\Portal Loccitane 2.0\Portal\Portal.UI\Uploads\ControlShop\Error\" + Path.GetFileName(ArquivoZip));
                            System.IO.File.Move(ArquivoZip, @"C:\Portal Loccitane 2.0\Portal\Portal.UI\Uploads\ControlShop\Error\" + Path.GetFileName(ArquivoZip));
                        }

                        _LjControleShopControle.ARQUIVO = "/Uploads/ControlShop/Error/" + Path.GetFileName(ArquivoZip);
                        _LjControlShopRetorno.STATUS_RETORNO = Retorno;
                        _LjControleShopControle.LJ_CONTROLSHOP_RETORNOs.Add(_LjControlShopRetorno);

                        _rptControlShopCtrl.Adicionar(_LjControleShopControle);
                    }
                }
            }
        }
    }
}
