﻿using Portal.Core.Data;
using Portal.Core.Entidades.Comercial;
using Portal.Core.Entidades.DB_INTERFACES;

namespace Portal.Core.Repositorios.Comercial
{
    public class rptIN_INTERFACE_PARAMETER : Repositorio<IN_INTERFACE_PARAMETER>, irptIN_INTERFACE_PARAMETER
    {
        public rptIN_INTERFACE_PARAMETER() : base(new PortalDataContext(2)) { }

    }
}
