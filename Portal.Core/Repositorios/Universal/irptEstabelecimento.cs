﻿using Portal.Core.Entidades.Universal;

namespace Portal.Core.Repositorios.Universal
{
    public interface irptEstabelecimento : IRepositorio<ESTABELECIMENTO>
    {}
}
