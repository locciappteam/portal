﻿using Portal.Core.Data;
using Portal.Core.Entidades.Universal;

namespace Portal.Core.Repositorios.Universal
{
    public class rptEstado:Repositorio<ESTADO>, irptEstado
    {
        public rptEstado() : base(new PortalDataContext())
        {

        }
    }
}
