﻿using Portal.Core.Data;
using Portal.Core.Entidades.Comercial;

namespace Portal.Core.Repositorios.Comercial
{
    public class rptGerenteDistrital : Repositorio<CM_GERENTE_DISTRITAL>, irptGerenteDistrital
    {
        public rptGerenteDistrital():base( new PortalDataContext()){}

    }
}
