﻿using Portal.Core.Data;
using Portal.Core.Entidades.Comercial;

namespace Portal.Core.Repositorios.Comercial
{
    public class rptRegional : Repositorio<CM_REGIONAL>, irptRegional
    {
        public rptRegional() : base(new PortalDataContext()) { }

    }
}
