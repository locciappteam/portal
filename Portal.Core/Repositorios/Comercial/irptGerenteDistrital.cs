﻿using Portal.Core.Entidades.Comercial;

namespace Portal.Core.Repositorios.Comercial
{
    public interface irptGerenteDistrital : IRepositorio<CM_GERENTE_DISTRITAL>
    {}
}
