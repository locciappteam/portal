﻿using Portal.Core.Data;
using Portal.Core.Entidades.Comercial;

namespace Portal.Core.Repositorios.Comercial
{
    public class rptGerenteLoja : Repositorio<CM_GERENTE_LOJA>, irptGerenteLoja
    {
        public rptGerenteLoja() : base(new PortalDataContext()) { }

    }
}
