﻿using Portal.Core.Entidades.Comercial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Repositorios.Comercial
{
    public interface irptGerenteLoja : IRepositorio<CM_GERENTE_LOJA>
    {}
}
