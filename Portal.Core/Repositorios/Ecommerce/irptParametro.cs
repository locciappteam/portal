﻿using Portal.Core.Entidades.Ecommerce;

namespace Portal.Core.Repositorios.Ecommerce
{
    public interface irptParametro : IRepositorio<EC_PARAMETRO>
    {}
}
