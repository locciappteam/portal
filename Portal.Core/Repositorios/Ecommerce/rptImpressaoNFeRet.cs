﻿using Portal.Core.Data;
using Portal.Core.Entidades.Ecommerce;

namespace Portal.Core.Repositorios.Ecommerce
{
    public class rptImpressaoNFeRet : Repositorio<EC_IMPRESSAO_NFE_RETORNO>, irptImpressaoNFeRet
    {
        public rptImpressaoNFeRet():base(new PortalDataContext())
        {

        }
    }
}
