﻿using Portal.Core.Data;
using Portal.Core.Entidades.Ecommerce;

namespace Portal.Core.Repositorios.Ecommerce
{
    public class rptParametro : Repositorio<EC_PARAMETRO>, irptParametro
    {

        public rptParametro() : base(new PortalDataContext())
        {

        }
    }
}
