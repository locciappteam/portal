﻿using Portal.Core.Data;
using Portal.Core.Entidades.Ecommerce;

namespace Portal.Core.Repositorios.Ecommerce
{
    public class rptImpressaoNFeCtrl : Repositorio<EC_IMPRESSAO_NFE_CONTROLE>, irptImpressaoNFeCtrl
    {

        public rptImpressaoNFeCtrl() : base(new PortalDataContext())
        {

        }
    }
}
