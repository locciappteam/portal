﻿using Portal.Core.Entidades.Ecommerce;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Repositorios.Ecommerce
{
    public interface irptPedidoGiftcard : IRepositorio<EC_PEDIDO_GIFTCARD>
    {}
}
