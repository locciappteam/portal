﻿using Portal.Core.Data;
using Portal.Core.Entidades.Ecommerce;

namespace Portal.Core.Repositorios.Ecommerce
{
    public class rptPedidoGiftcard : Repositorio<EC_PEDIDO_GIFTCARD>, irptPedidoGiftcard
    {
        public rptPedidoGiftcard() : base(new PortalDataContext())
        {

        }
    }
}
