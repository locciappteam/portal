﻿using Portal.Core.Data;
using Portal.Core.Entidades.GerenciamentoFiscal;

namespace Portal.Core.Repositorios.GerenciamentoFiscal
{
    public class rptISS : Repositorio<FS_APURACAO_ISS>, irptISS
    {
        public rptISS() : base(new PortalDataContext())
        {

        }
    }
}
