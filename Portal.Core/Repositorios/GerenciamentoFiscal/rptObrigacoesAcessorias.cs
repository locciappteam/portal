﻿using Portal.Core.Data;
using Portal.Core.Entidades.GerenciamentoFiscal;

namespace Portal.Core.Repositorios.GerenciamentoFiscal
{
    public class rptObrigacoesAcessorias:Repositorio<FS_CTRL_OBRIGACOES>, irptObrigacoesAcessorias
    {
        public rptObrigacoesAcessorias() : base(new PortalDataContext())
        {

        }
    }
}
