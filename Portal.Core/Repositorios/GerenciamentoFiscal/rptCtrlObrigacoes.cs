﻿using Portal.Core.Data;
using Portal.Core.Entidades.GerenciamentoFiscal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Repositorios.GerenciamentoFiscal
{
    public class rptCtrlObrigacoes : Repositorio<FS_CTRL_OBRIGACOES>, irptCtrlObrigacoes
    {
        public rptCtrlObrigacoes() : base(new PortalDataContext())
        {

        }
    }
}
