﻿using Portal.Core.Data;
using Portal.Core.Entidades.GerenciamentoFiscal;

namespace Portal.Core.Repositorios.GerenciamentoFiscal
{
    public class rptIPI : Repositorio<FS_APURACAO_IPI>, irptIPI
    {
        public rptIPI() : base(new PortalDataContext())
        {

        }
    }
}
