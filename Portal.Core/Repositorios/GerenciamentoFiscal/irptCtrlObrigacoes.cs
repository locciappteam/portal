﻿using Portal.Core.Entidades.GerenciamentoFiscal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Repositorios.GerenciamentoFiscal
{
    public interface irptCtrlObrigacoes : IRepositorio<FS_CTRL_OBRIGACOES>
    {}
}
