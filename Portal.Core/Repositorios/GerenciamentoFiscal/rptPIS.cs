﻿using Portal.Core.Data;
using Portal.Core.Entidades.GerenciamentoFiscal;

namespace Portal.Core.Repositorios.GerenciamentoFiscal
{
    public class rptPIS : Repositorio<FS_APURACAO_PIS>, irptPIS
    {
        public rptPIS() : base(new PortalDataContext())
        {

        }
    }
}
