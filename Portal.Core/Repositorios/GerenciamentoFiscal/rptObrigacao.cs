﻿using Portal.Core.Data;
using Portal.Core.Entidades.GerenciamentoFiscal;

namespace Portal.Core.Repositorios.GerenciamentoFiscal
{
    public class rptObrigacao : Repositorio<FS_OBRIGACAO>, irptObrigacao
    {
        public rptObrigacao() : base(new PortalDataContext())
        {

        }
    }
}
