﻿using Portal.Core.Data;
using Portal.Core.Entidades.GerenciamentoFiscal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Repositorios.GerenciamentoFiscal
{
    public class rptCofins : Repositorio<FS_APURACAO_COFINS>, irptCofins
    {
        public rptCofins() : base(new PortalDataContext())
        {

        }
    }
}
