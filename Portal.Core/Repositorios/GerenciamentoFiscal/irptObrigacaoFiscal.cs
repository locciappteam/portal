﻿using Portal.Core.Entidades.GerenciamentoFiscal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Repositorios.GerenciamentoFiscal
{
    public interface irptObrigacaoFiscal : IRepositorio<FS_OBRIGACAO_FISCAL>
    {}
}
