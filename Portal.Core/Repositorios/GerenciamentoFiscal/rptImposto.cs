﻿using Portal.Core.Data;
using Portal.Core.Entidades.GerenciamentoFiscal;

namespace Portal.Core.Repositorios.GerenciamentoFiscal
{
    public class rptImposto : Repositorio<FS_IMPOSTO>, irptImposto
    {
        public rptImposto() : base(new PortalDataContext())
        {

        }
    }
}
