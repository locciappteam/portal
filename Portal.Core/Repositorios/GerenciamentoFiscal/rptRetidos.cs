﻿using Portal.Core.Data;
using Portal.Core.Entidades.GerenciamentoFiscal;

namespace Portal.Core.Repositorios.GerenciamentoFiscal
{
    public class rptRetidos : Repositorio<FS_APURACAO_RETIDOS>, irptRetidos
    {
        public rptRetidos() : base(new PortalDataContext())
        {

        }
    }
}
