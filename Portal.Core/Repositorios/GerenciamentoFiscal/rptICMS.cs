﻿using Portal.Core.Data;
using Portal.Core.Entidades.GerenciamentoFiscal;

namespace Portal.Core.Repositorios.GerenciamentoFiscal
{
    public class rptICMS : Repositorio<FS_APURACAO_ICMS>, irptICMS
    {
        public rptICMS() : base(new PortalDataContext())
        {

        }
    }
}
