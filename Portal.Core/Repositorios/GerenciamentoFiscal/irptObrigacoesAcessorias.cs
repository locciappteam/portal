﻿using Portal.Core.Entidades.GerenciamentoFiscal;

namespace Portal.Core.Repositorios.GerenciamentoFiscal
{
    public interface irptObrigacoesAcessorias : IRepositorio<FS_CTRL_OBRIGACOES>
    {}
}
