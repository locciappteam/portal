﻿using Portal.Core.Data;
using Portal.Core.Entidades.GerenciamentoFiscal;

namespace Portal.Core.Repositorios.GerenciamentoFiscal
{
    public class rptObrigacaoFiscal : Repositorio<FS_OBRIGACAO_FISCAL>, irptObrigacaoFiscal
    {
        public rptObrigacaoFiscal() : base(new PortalDataContext())
        {

        }
    }
}
