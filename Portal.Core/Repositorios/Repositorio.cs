﻿using Portal.Core.Data;
using Portal.Core.Entidades;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Portal.Core.Repositorios
{
    public class Repositorio<T> : IRepositorio<T> where T : ENTIDADE_DEFAULT
    {
        protected readonly DbContext _ctx;

        public Repositorio(DbContext context)
        {
            _ctx = context;
        }
        private void Salvar()
        {
            _ctx.SaveChanges();
        }

        public T Adicionar(T entidade)
        {
            _ctx.Set<T>().Add(entidade);
            Salvar();
            return entidade;
        }

        public IEnumerable<T> AdicionarList(IEnumerable<T> entidade)
        {
            _ctx.Set<T>().AddRange(entidade);
            Salvar();
            return entidade;
        }

        public T Editar(T entidade)
        {
            _ctx.Entry(entidade).State = EntityState.Modified;
            Salvar();
            return entidade;
        }

        public void Excluir(T entidade)
        {
            //_ctx.Entry(entidade).State = EntityState.Deleted;
            _ctx.Set<T>().Attach(entidade);
            _ctx.Set<T>().Remove(entidade);
            Salvar();
        }

        public IEnumerable<T> Obter()
        {
            return _ctx.Set<T>().ToList();
        }

        public T ObterPorID(object id)
        {
            return _ctx.Set<T>().Find(id);
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
