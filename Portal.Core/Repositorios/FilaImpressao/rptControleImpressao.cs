﻿using Portal.Core.Data;
using Portal.Core.Entidades.FilaImpressao;

namespace Portal.Core.Repositorios.FilaImpressao
{
    public class rptControleImpressao : Repositorio<FI_CONTROLE_IMPRESSAO>, irptControleImpressao
    {
        public rptControleImpressao() : base( new PortalDataContext()){}
    }
}
