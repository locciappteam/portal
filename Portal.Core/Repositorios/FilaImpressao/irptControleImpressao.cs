﻿using Portal.Core.Entidades.FilaImpressao;

namespace Portal.Core.Repositorios.FilaImpressao
{
    public interface irptControleImpressao : IRepositorio<FI_CONTROLE_IMPRESSAO>
    { }
}
