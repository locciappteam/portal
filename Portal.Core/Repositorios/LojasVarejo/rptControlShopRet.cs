﻿using Portal.Core.Data;
using Portal.Core.Entidades.LojasVarejo;

namespace Portal.Core.Repositorios.LojasVarejo
{
    public class rptControlShopRet : Repositorio<LJ_CONTROLSHOP_RETORNO>, irptControlShopRet
    {
        public rptControlShopRet() : base(new PortalDataContext())
        {

        }
    }
}
