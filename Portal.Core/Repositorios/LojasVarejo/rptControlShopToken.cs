﻿using Portal.Core.Data;
using Portal.Core.Entidades.LojasVarejo;

namespace Portal.Core.Repositorios.LojasVarejo
{
    public class rptControlShopToken : Repositorio<LJ_CONTROLSHOP_TOKEN>, irptControlShopToken
    {
        public rptControlShopToken() : base(new PortalDataContext())
        {

        } }
}
