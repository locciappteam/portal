﻿using Portal.Core.Entidades.LojasVarejo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Repositorios.LojasVarejo
{
    public interface irptControlShopToken : IRepositorio<LJ_CONTROLSHOP_TOKEN>
    {}
}
