﻿using Portal.Core.Data;
using Portal.Core.Entidades.LojasVarejo;

namespace Portal.Core.Repositorios.LojasVarejo
{
    public class rptControlShopCtrl:Repositorio<LJ_CONTROLSHOP_CONTROLE>, irptControlShopCtrl
    {
        public rptControlShopCtrl() : base(new PortalDataContext())
        {

        }
    }
}
