﻿using Portal.Core.Data;
using Portal.Core.Entidades.Portal;

namespace Portal.Core.Repositorios.Portal
{
    public class rptPortalPagina : Repositorio<PORTAL_PAGINA>, irptPortalPagina
    {
        public rptPortalPagina() : base(new PortalDataContext())
        {

        }
    }
}
