﻿using Portal.Core.Data;
using Portal.Core.Entidades.Portal;

namespace Portal.Core.Repositorios.Portal
{
    public class rptPortalDepartamento : Repositorio<PORTAL_DEPARTAMENTO>, irptPortalDepartamento
    {
        public rptPortalDepartamento():base( new PortalDataContext()){}
    }
}
