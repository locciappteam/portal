﻿using Portal.Core.Data;
using Portal.Core.Entidades.Portal;

namespace Portal.Core.Repositorios.Portal
{
    public class rptPortalMenu : Repositorio<PORTAL_MENU>, irptPortalMenu
    {
        public rptPortalMenu() : base(new PortalDataContext())
        {

        }
    }
}
