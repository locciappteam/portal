﻿using Portal.Core.Entidades.Portal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Repositorios.Portal
{
    public interface irptPortalDepartamento : IRepositorio<PORTAL_DEPARTAMENTO>
    {}
}
