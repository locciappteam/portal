﻿using Portal.Core.Data;
using Portal.Core.Entidades.Portal;

namespace Portal.Core.Repositorios.Portal
{
    public class rptPortalCtrlAcesso : Repositorio<PORTAL_CTRL_ACESSO>, irptPortalCtrlAcesso
    {
        public rptPortalCtrlAcesso() : base(new PortalDataContext())
        {

        }
    }
}
