﻿using Portal.Core.Data;
using Portal.Core.Entidades.Portal;
using Portal.Core.Entidades.Universal;
using System.Linq;

namespace Portal.Core.Repositorios.Portal
{
    public class rptPortalUsuario : Repositorio<PORTAL_USUARIO>, irptPortalUsuario
    {
        public rptPortalUsuario() : base(new PortalDataContext())
        {

        }
        public PORTAL_USUARIO Autenticar(string Login, string Senha)
        {
            Senha = MD5HASH.CriptografarSenha(Senha);
            return _ctx.Set<PORTAL_USUARIO>().FirstOrDefault(usuario => usuario.LOGIN == Login && usuario.SENHA == Senha && usuario.ATIVADO == true);
        }
    }
}
