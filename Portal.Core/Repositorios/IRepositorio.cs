﻿using Portal.Core.Entidades;
using System;
using System.Collections.Generic;

namespace Portal.Core.Repositorios
{
    public interface IRepositorio<T>:IDisposable where T:ENTIDADE_DEFAULT
    {
        T Adicionar(T entidade);
        IEnumerable<T> AdicionarList(IEnumerable<T> entidade);
        T Editar(T entidade);
        void Excluir(T entidade);
        IEnumerable<T> Obter();
        T ObterPorID(object id);
    }
}
