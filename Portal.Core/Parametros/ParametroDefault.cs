﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Parametros
{
    public class ParametroDefault
    {
        public ParametroDefault()
        {
            Estabelecimento = new List<int>();
            UF = new List<int>();
            Terminal = new List<string>();
        }

        public List<int> Estabelecimento { get; set; }
        public List<int> UF { get; set; }
        public List<string> Terminal { get; set; }
        public DateTime DataInicial { get; set; }
        public DateTime DataFinal { get; set; }
    }
}
