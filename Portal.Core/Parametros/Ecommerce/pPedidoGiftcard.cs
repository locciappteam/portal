﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Parametros.Ecommerce
{
    public class pPedidoGiftcard : ParametroDefault
    {
        public string Pedido { get; set; }
        public string Giftcard { get; set; }
    }
}
