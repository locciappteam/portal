﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Parametros.Ecommerce
{
    public class pImpressaoNFe : ParametroDefault
    {
        public string ChaveNFe { get; set; }
        public int? NumeroNF { get; set; }
        public string SerieNF { get; set; }
    }
}
