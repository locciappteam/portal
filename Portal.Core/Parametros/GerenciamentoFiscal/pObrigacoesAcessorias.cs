﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Parametros.GerenciamentoFiscal
{
    public class pObrigacoesAcessorias : ParametroDefault
    {
        public pObrigacoesAcessorias()
        {
            Obrigacoes = new List<int>();
            StatusObrigacao = new List<string>();
        }

        public List<int> Obrigacoes { get; set; }
        public List<string> StatusObrigacao { get; set; }
    }
}