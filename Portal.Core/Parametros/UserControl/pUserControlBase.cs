﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Parametros.UserControl
{
    public class pUserControlBase
    {
        public string ID { get; set; }
        public string Text { get; set; }
        public string Icon { get; set; }
        public int Col { get; set; }
        public string Mask { get; set; }
    }
}
