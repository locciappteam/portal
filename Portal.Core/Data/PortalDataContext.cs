﻿using Portal.Core.Entidades.Comercial;
using Portal.Core.Entidades.Ecommerce;
using Portal.Core.Entidades.FilaImpressao;
using Portal.Core.Entidades.GerenciamentoFiscal;
using Portal.Core.Entidades.LojasVarejo;
using Portal.Core.Entidades.Portal;
using Portal.Core.Entidades.Universal;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Portal.Core.Data
{
    public class PortalDataContext : DbContext
    {
        private string _connStr { get; set; }

        public PortalDataContext(int dbMode = 1)
           // :base(@"Data Source=AMSAOCBRDB01;Initial Catalog=PORTAL_LOCCITANE;Persist Security Info=True;User ID=portalloccitane;Password=3u3NO483")
        {
            switch (dbMode)
            {
                case 1:
                    _connStr = @"Data Source=AMSAOCBRDB01;Initial Catalog=PORTAL_LOCCITANE;Persist Security Info=True;User ID=portalloccitane;Password=3u3NO483";
                    break;

                case 2:
                    _connStr = @"Data Source=AMSAOCBRDB01;Initial Catalog=DB_INTERFACES;Persist Security Info=True;User ID=portalloccitane;Password=3u3NO483";
                    break;
            }

            this.Database.Connection.ConnectionString = _connStr;
        }

        //DbSet UNIVERSAL =============================================================================
        public DbSet<ESTADO> Estados { get; set; }
        public DbSet<ESTABELECIMENTO> Estabelecimentos { get; set; }

        //DbSet PORTAL ================================================================================
        public DbSet<PORTAL_DEPARTAMENTO> PortalDepartamento { get; set; }
        public DbSet<PORTAL_USUARIO> PortalUsuario { get; set; }
        public DbSet<PORTAL_MENU> PortalMenu { get; set; }
        public DbSet<PORTAL_PAGINA> PortalPagina { get; set; }
        public DbSet<PORTAL_CTRL_ACESSO> PortalCtrlAcesso { get; set; }

        //DbSet LOJAS VAREJO ==========================================================================
        public DbSet<LJ_CONTROLSHOP_TOKEN> LjControlShopToken { get; set; }
        public DbSet<LJ_CONTROLSHOP_CONTROLE> LjControlShopCtrl { get; set; }
        public DbSet<LJ_CONTROLSHOP_RETORNO> LjControlShopRetorno { get; set; }

        //DbSet COMERCIAL =============================================================================
        public DbSet<CM_REGIONAL> CmRegional { get; set; }
        public DbSet<CM_GERENTE_DISTRITAL> CmGerenteDistrital { get; set; }
        public DbSet<CM_GERENTE_LOJA> CmGerenteLoja { get; set; }

        //DbSet GERENCIAMENTO FISCAL ==================================================================
        public DbSet<FS_APURACAO_COFINS> FsApuracaoCofins { get; set; }
        public DbSet<FS_APURACAO_ICMS> FsApuracaoICMS { get; set; }
        public DbSet<FS_APURACAO_ICMSST> FsApuracaoICMSST { get; set; }
        public DbSet<FS_APURACAO_IPI> FsApuracaoIPI { get; set; }
        public DbSet<FS_APURACAO_ISS> FsApuracaoISS { get; set; }
        public DbSet<FS_APURACAO_PIS> FsApuracaoPIS { get; set; }
        public DbSet<FS_APURACAO_RETIDOS> FsApuracaoRetidos { get; set; }
        public DbSet<FS_CTRL_OBRIGACOES> FsCtrlObrigacoes { get; set; }
        public DbSet<FS_IMPOSTO> FsImposto { get; set; }
        public DbSet<FS_OBRIGACAO> FsObrigacao { get; set; }
        public DbSet<FS_OBRIGACAO_FISCAL> FsObrigacaoFiscal { get; set; }

        //DbSet E-COMMERCE ============================================================================
        public DbSet<FI_CONTROLE_IMPRESSAO> FiControleImpressao { get; set; }
        public DbSet<EC_IMPRESSAO_NFE_CONTROLE> EcImpressaoNfeCtrl { get; set; }
        public DbSet<EC_IMPRESSAO_NFE_RETORNO> EcImpressaoNfeRetorno{ get; set; }
        public DbSet<EC_PEDIDO_GIFTCARD> EcPedidoGiftCard { get; set; }
        public DbSet<EC_PARAMETRO> EcParametro { get; set; }


        //Remove Plural das Entidades =================================================================

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<FS_OBRIGACAO_FISCAL>()
                .HasKey(a => new { a.ID, a.CTRLOBRIGACOESID });

            modelBuilder.Entity<FS_APURACAO_ICMSST>()
                .HasKey(a => new { a.ID, a.CTRLOBRIGACOESID });

            modelBuilder.Entity<FS_APURACAO_RETIDOS>()
                .HasKey(a => new { a.ID, a.CTRLOBRIGACOESID });

            modelBuilder.Entity<EC_PARAMETRO>().HasKey(p => p.PARAMETRO);

            modelBuilder.Entity<FI_CONTROLE_IMPRESSAO>().HasKey(p=> p.ID);
        }
        //Remove Plural das Entidades =================================================================
    }
}