﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Portal.Core.XML
{
    [XmlRoot(ElementName = "eformsInserirRetorno", Namespace = "http://www.nddigital.com.br/connector")]
    public class ImpressaoNFeEformsInserirRetorno
    {
        [XmlElement(ElementName = "versao", Namespace = "http://www.nddigital.com.br/connector")]
        public string Versao { get; set; }
        [XmlElement(ElementName = "protocolo", Namespace = "http://www.nddigital.com.br/connector")]
        public string Protocolo { get; set; }
        [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsd { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot(ElementName = "eDocumentResult", Namespace = "http://nddigital.com.br/eForms/webservices")]
    public class ImpressaoNFeEDocumentResult
    {
        [XmlElement(ElementName = "eformsInserirRetorno", Namespace = "http://www.nddigital.com.br/connector")]
        public ImpressaoNFeEformsInserirRetorno EformsInserirRetorno { get; set; }
    }

    [XmlRoot(ElementName = "eDocumentResponse", Namespace = "http://nddigital.com.br/eForms/webservices")]
    public class ImpressaoNFeEDocumentResponse
    {
        [XmlElement(ElementName = "eDocumentResult", Namespace = "http://nddigital.com.br/eForms/webservices")]
        public ImpressaoNFeEDocumentResult EDocumentResult { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class ImpressaoNFeBody
    {
        [XmlElement(ElementName = "eDocumentResponse", Namespace = "http://nddigital.com.br/eForms/webservices")]
        public ImpressaoNFeEDocumentResponse EDocumentResponse { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class ImpressaoNFeEnvelope
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public ImpressaoNFeBody Body { get; set; }
        [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsd { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soap { get; set; }
    }
}
