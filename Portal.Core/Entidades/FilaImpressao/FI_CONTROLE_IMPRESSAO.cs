﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.Core.Entidades.FilaImpressao
{
    [Table("FI_CONTROLE_IMPRESSAO")]
    public class FI_CONTROLE_IMPRESSAO : ENTIDADE_DEFAULT
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public DateTime? DATA_SOLICITACAO { get; set; }
        public DateTime? DATA_IMPRESSAO { get; set; }
        public string REF_INTERNA { get; set; }
        public byte? IDIMPRESSORA { get; set; }
        public byte? STATUS { get; set; }
    }
}
