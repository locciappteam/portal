﻿using Newtonsoft.Json;
using Portal.Core.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.Core.Entidades.GerenciamentoFiscal
{
    public class FS_OBRIGACAO : ENTIDADE_DEFAULT
    {
        public FS_OBRIGACAO()
        {
            DATA_ALTERACAO = DateTime.Now;
            FS_OBRIGACAO_FISCALs = new List<FS_OBRIGACAO_FISCAL>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required(ErrorMessage = "Informe a Descrição!")]
        public string DESCRICAO { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        [JsonIgnore]
        public virtual ICollection<FS_OBRIGACAO_FISCAL> FS_OBRIGACAO_FISCALs { get; set; }
    }
}
