﻿using Newtonsoft.Json;
using Portal.Core.Entidades;
using Portal.Core.Entidades.Universal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.Core.Entidades.GerenciamentoFiscal
{
    public class FS_APURACAO_ICMSST : ENTIDADE_DEFAULT
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int? CTRLOBRIGACOESID { get; set; }
        [ForeignKey("CTRLOBRIGACOESID")]
        [JsonIgnore]
        public virtual FS_CTRL_OBRIGACOES FS_CTRL_OBRIGACOES { get; set; }

        public int? ESTADOID { get; set; }
        [ForeignKey("ESTADOID")]
        public virtual ESTADO ESTADO { get; set; }

        public decimal SALDO_ANTERIOR { get; set; }
        public decimal CREDITO { get; set; }
        public decimal DEBITO { get; set; }
        public decimal LANCAMENTOS { get; set; }
        public decimal VALOR_SALDO { get; set; }
    }
}
