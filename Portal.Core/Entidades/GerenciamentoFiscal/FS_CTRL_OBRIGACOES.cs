﻿using Newtonsoft.Json;
using Portal.Core.Entidades;
using Portal.Core.Entidades.Universal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.Core.Entidades.GerenciamentoFiscal
{
    public class FS_CTRL_OBRIGACOES : ENTIDADE_DEFAULT
    {
        public FS_CTRL_OBRIGACOES()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required(ErrorMessage = "Informe o estabelecimento!")]
        public int ESTABELECIMENTOID { get; set; }
        [ForeignKey("ESTABELECIMENTOID")]
        public virtual ESTABELECIMENTO ESTABELECIMENTO { get; set; }

        [Required(ErrorMessage = "Informe a data da apuração!")]
        public DateTime DATA_APURACAO { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual FS_APURACAO_COFINS FS_APURACAO_COFINS { get; set; }
        public virtual FS_APURACAO_ICMS FS_APURACAO_ICMS { get; set; }
        public virtual ICollection<FS_APURACAO_ICMSST> FS_APURACAO_ICMSSTs { get; set; }
        public virtual FS_APURACAO_IPI FS_APURACAO_IPI { get; set; }
        public virtual FS_APURACAO_ISS FS_APURACAO_ISS { get; set; }
        public virtual FS_APURACAO_PIS FS_APURACAO_PIS { get; set; }
        public virtual ICollection<FS_APURACAO_RETIDOS> FS_APURACAO_RETIDOSs { get; set; }
        public virtual ICollection<FS_OBRIGACAO_FISCAL> FS_OBRIGACAO_FISCALs { get; set; }
    }
}
