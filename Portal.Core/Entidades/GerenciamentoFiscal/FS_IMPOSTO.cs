﻿using Newtonsoft.Json;
using Portal.Core.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Entidades.GerenciamentoFiscal
{
    public class FS_IMPOSTO : ENTIDADE_DEFAULT
    {
        public FS_IMPOSTO()
        {
            DATA_ALTERACAO = DateTime.Now;
            FS_APURACAO_RETIDOSs = new List<FS_APURACAO_RETIDOS>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(15, ErrorMessage = "Informe a Sigla com até {1} caracteres!")]
        public string SIGLA { get; set; }

        [Required (ErrorMessage = "Informe uma Descrição!")]
        public string DESCRICAO { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        [JsonIgnore]
        public virtual ICollection<FS_APURACAO_RETIDOS> FS_APURACAO_RETIDOSs { get; set; }
    }
}
