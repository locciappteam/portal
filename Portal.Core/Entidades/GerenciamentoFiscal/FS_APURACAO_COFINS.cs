﻿using Newtonsoft.Json;
using Portal.Core.Entidades;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.Core.Entidades.GerenciamentoFiscal
{
    public class FS_APURACAO_COFINS: ENTIDADE_DEFAULT
    {
        [Key, ForeignKey("FS_CTRL_OBRIGACOES")]
        public int ID { get; set; }

        public decimal SALDO_ANTERIOR { get; set; }
        public decimal SAIDAS_DEBITO { get; set; }
        public decimal OUTROS_DEBITO { get; set; }
        public decimal ENTRADAS_CREDITO { get; set; }
        public decimal OUTROS_CREDITO { get; set; }
        public decimal SALDO_CREDOR { get; set; }
        public decimal SALDO_DEVEDOR { get; set; }

        [JsonIgnore]
        public virtual FS_CTRL_OBRIGACOES FS_CTRL_OBRIGACOES { get; set; }
    }
}