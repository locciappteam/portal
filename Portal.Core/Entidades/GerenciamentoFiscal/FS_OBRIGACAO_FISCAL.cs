﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.Core.Entidades.GerenciamentoFiscal
{
    public class FS_OBRIGACAO_FISCAL : ENTIDADE_DEFAULT
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int? CTRLOBRIGACOESID { get; set; }
        [ForeignKey("CTRLOBRIGACOESID")]
        [JsonIgnore]
        public virtual FS_CTRL_OBRIGACOES FS_CTRL_OBRIGACOES { get; set; }

        public int OBRIGACAOID { get; set; }
        [ForeignKey("OBRIGACAOID")]
        public virtual FS_OBRIGACAO FS_OBRIGACAO { get; set; }

        public string STATUS { get; set; }
        public string OBSERVACAO { get; set; }
    }
}
