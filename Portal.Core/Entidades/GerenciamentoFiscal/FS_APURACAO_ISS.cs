﻿using Newtonsoft.Json;
using Portal.Core.Entidades;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.Core.Entidades.GerenciamentoFiscal
{
    public class FS_APURACAO_ISS : ENTIDADE_DEFAULT
    {
        [Key, ForeignKey("FS_CTRL_OBRIGACOES")]
        public int ID { get; set; }

        public decimal TOMADOS { get; set; }
        public decimal PRESTADOS { get; set; }

        [JsonIgnore]
        public virtual FS_CTRL_OBRIGACOES FS_CTRL_OBRIGACOES { get; set; }
    }
}
