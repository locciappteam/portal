﻿using Newtonsoft.Json;
using Portal.Core.Entidades;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.Core.Entidades.GerenciamentoFiscal
{
    public class FS_APURACAO_RETIDOS : ENTIDADE_DEFAULT
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int? CTRLOBRIGACOESID { get; set; }
        [ForeignKey("CTRLOBRIGACOESID")]
        [JsonIgnore]
        public virtual FS_CTRL_OBRIGACOES FS_CTRL_OBRIGACOES { get; set; }

        public int IMPOSTOID { get; set; }
        [ForeignKey("IMPOSTOID")]
        public virtual FS_IMPOSTO FS_IMPOSTO { get; set; }

        public decimal VALOR_IMPOSTO { get; set; }
    }
}
