﻿using Portal.Core.Data;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Portal.Core.Entidades
{
    public class ControlShop
    {
        ViewsDataContext _views = new ViewsDataContext();

        public ICollection<W_CONTROLSHOP_XML> ObterXML(int EstabelecimentoId, DateTime DataInicial, DateTime DataFinal)
        {
            ICollection<W_CONTROLSHOP_XML> XMLs = (
                    from
                        a in _views.W_CONTROLSHOP_XMLs
                    where
                        a.ESTABELECIMENTOID == EstabelecimentoId
                        && a.DATA_VENDA >= DataInicial
                        && a.DATA_VENDA <= DataFinal
                    select
                        a
                ).ToList();

            return XMLs;
        }

        public void GerarXML(ICollection<W_CONTROLSHOP_XML> XMLs, string CaminhoXML)
        {
            XmlDocument docXML = new XmlDocument();
            foreach (var XML in XMLs)
            {
                docXML.LoadXml(XML.ARQUIVO_XML.ToString());

                var ChaveNFe = docXML.SelectSingleNode("//@Id").Value;

                docXML.Save(CaminhoXML + ChaveNFe + ".xml");
            }
        }

        public string ZiparXML(ICollection<W_CONTROLSHOP_XML> XMLs, string CaminhoXML)
        {
            string ArquivoZip = CaminhoXML.Replace("XMLs\\", "") + XMLs.Select(a => a.CNPJ).FirstOrDefault() + "_" + DateTime.Today.ToString("yyyyMMdd") + ".zip";

            if (!System.IO.File.Exists(ArquivoZip))
            {
                ZipFile.CreateFromDirectory(
                    CaminhoXML,
                    ArquivoZip
                );
            }
            else
            {
                System.IO.File.Delete(ArquivoZip);

                ZipFile.CreateFromDirectory(
                    CaminhoXML,
                    ArquivoZip
                );                
            }

            return ArquivoZip;
        }
    }
}
