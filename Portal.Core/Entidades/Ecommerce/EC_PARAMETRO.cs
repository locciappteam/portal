﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.Core.Entidades.Ecommerce
{
    [Table("EC_PARAMETROS")]
    public class EC_PARAMETRO : ENTIDADE_DEFAULT
    {
        public string PARAMETRO { get; set; }

        public string DESCRICAO { get; set; }

        public string TIPO { get; set; }

        public int? TAMANHO { get; set; }

        public string MASCARA { get; set; }

        public string VALOR { get; set; }

        public DateTime DATA_CRIACAO { get; set; }

        public DateTime DATA_ALTERACAO { get; set; }
    }
}
