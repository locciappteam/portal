﻿using Portal.Core.Entidades.Universal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.Core.Entidades.Ecommerce
{
    public class EC_IMPRESSAO_NFE_CONTROLE : ENTIDADE_DEFAULT
    {
        public EC_IMPRESSAO_NFE_CONTROLE()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int ESTABELECIMENTOID { get; set; }
        [ForeignKey("ESTABELECIMENTOID")]
        public virtual ESTABELECIMENTO ESTABELECIMENTO { get; set; }

        public string PEDIDO { get; set; }
        public string CHAVE_NFE { get; set; }
        public int? NUMERO_NF { get; set; }
        public string SERIE_NF { get; set; }
        public DateTime? DATA_EMISSAO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ICollection<EC_IMPRESSAO_NFE_RETORNO> EC_IMPRESSAO_NFE_RETORNOs { get; set; }
    }
}
