﻿using Portal.Core.Entidades.Universal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.Core.Entidades.Comercial
{
    public class CM_REGIONAL : ENTIDADE_DEFAULT
    {
        public CM_REGIONAL()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Campo obrigatório, (Max: {0})")]
        public string REGIONAL { get; set; }

        public bool ATIVADO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ICollection<CM_GERENTE_DISTRITAL> CM_GERENTE_DISTRITALs { get; set; }
        public virtual ICollection<ESTABELECIMENTO> PORTAL_ESTABELECIMENTOs { get; set; }
    }
}
