﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Entidades.Comercial
{
    public class CM_GERENTE_DISTRITAL : ENTIDADE_DEFAULT
    {
        public CM_GERENTE_DISTRITAL()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int REGIONALID { get; set; }
        [ForeignKey("REGIONALID")]
        public virtual CM_REGIONAL CM_REGIONAL { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Campo obrigatório, (Max: {0})")]
        public string NOME { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(50, ErrorMessage = "Campo obrigatório, (Max: {0})")]
        public string EMAIL { get; set; }

        public bool ATIVADO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
    }
}
