﻿using Portal.Core.Entidades.Universal;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.Core.Entidades.Comercial
{
    public class CM_GERENTE_LOJA : ENTIDADE_DEFAULT
    {
        public CM_GERENTE_LOJA()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int ESTABELECIMENTOID { get; set; }
        [ForeignKey("ESTABELECIMENTOID")]
        public virtual ESTABELECIMENTO PORTAL_ESTABELECIMENTO { get; set; }

        [Required (ErrorMessage = "Campo obrigatório.")]
        public int MATRICULA { get; set; }

        [Required]
        [StringLength(75, ErrorMessage = "Campo obrigatório, (Max: {0})")]
        public string NOME { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(50, ErrorMessage = "Campo obrigatório, (Max: {0})")]
        public string EMAIL { get; set; }

        public bool ATIVADO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
    }
}
