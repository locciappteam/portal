﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Portal.Core.Entidades.DB_INTERFACES
{
    [Table("IN_INTERFACE_PARAMETER")]
    public class IN_INTERFACE_PARAMETER : ENTIDADE_DEFAULT
    {
        public int INTERFACE { get; set; }

        public string NAME { get; set; }

        public string DESCRIPTION { get; set; }

        public string TYPE { get; set; }

        public int? SIZE { get; set; }

        public string MASK { get; set; }

        public DateTime CREATED { get; set; }

        public DateTime UPDATED { get; set; }

        public string VALUE { get; set; }
    }
}
