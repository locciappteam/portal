﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Entidades.Portal
{
    public class PORTAL_DEPARTAMENTO : ENTIDADE_DEFAULT
    {
        public PORTAL_DEPARTAMENTO()
        {
            DATA_ALTERACAO = DateTime.Now;
        }
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(75, ErrorMessage = "Campo obrigatório, (Max: {0})")]
        public string NOME { get; set; }

        public bool ATIVADO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ICollection<PORTAL_USUARIO> PORTAL_USUARIOs { get; set; }
    }
}
