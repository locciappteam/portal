﻿using Portal.Core.Entidades.Universal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Entidades.Portal
{
    public class PORTAL_USUARIO : ENTIDADE_DEFAULT
    {
        public PORTAL_USUARIO()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Campo obrigatório, (Max: {0})")]
        public string LOGIN { get; set; }

        [Required(ErrorMessage = "Campo obrigatório.")]
        public string SENHA { get; set; }

        [NotMapped]
        [Required(ErrorMessage = "Campo obrigatório.")]
        [Compare("SENHA", ErrorMessage = "A senha não confere!")]
        public string SENHACONFIRMA { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(100, ErrorMessage = "Campo obrigatório, (Max: {0})")]
        public string EMAIL { get; set; }

        [Required]
        [StringLength(75, ErrorMessage = "Campo obrigatório, (Max: {0})")]
        public string NOME { get; set; }

        public string ENDERECO { get; set; }
        public int NUMERO { get; set; }
        public string BAIRRO { get; set; }
        public string CIDADE { get; set; }

        public int? ESTADOID { get; set; }
        [ForeignKey("ESTADOID")]
        public virtual ESTADO ESTADO { get; set; }

        public string CARGO { get; set; }

        public int DEPARTAMENTOID { get; set; }
        [ForeignKey("DEPARTAMENTOID")]
        public virtual PORTAL_DEPARTAMENTO PORTAL_DEPARTAMENTO { get; set; }

        public bool ATIVADO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
    }
}
