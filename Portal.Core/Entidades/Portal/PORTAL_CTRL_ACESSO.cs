﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Entidades.Portal
{
    public class PORTAL_CTRL_ACESSO : ENTIDADE_DEFAULT
    {
        public PORTAL_CTRL_ACESSO()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int USUARIOID { get; set; }
        [ForeignKey("USUARIOID")]
        public virtual PORTAL_USUARIO PORTAL_USUARIO { get; set; }

        public int PAGINAID { get; set; }
        [ForeignKey("PAGINAID")]
        public virtual PORTAL_PAGINA PORTAL_PAGINA { get; set; }

        public bool ADICIONAR { get; set; }
        public bool EDITAR { get; set; }
        public bool EXCLUIR { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
    }
}
