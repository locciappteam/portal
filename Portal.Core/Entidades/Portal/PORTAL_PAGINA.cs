﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Entidades.Portal
{
    public class PORTAL_PAGINA : ENTIDADE_DEFAULT
    {
        public PORTAL_PAGINA()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int MENUID { get; set; }
        [ForeignKey("MENUID")]
        public virtual PORTAL_MENU PORTAL_MENU { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Campo obrigatório, (Max: {0})")]
        public string NOME { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Campo obrigatório, (Max: {0})")]
        public string CONTROLLERNAME { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Campo obrigatório, (Max: {0})")]
        public string ACTIONNAME { get; set; }

        public string ICONE { get; set; }
        public bool ATIVADO { get; set; }
        public int ORDENAR { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ICollection<PORTAL_CTRL_ACESSO> PORTAL_CTRL_ACESSOs { get; set; }
    }
}
