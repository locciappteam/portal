﻿using Portal.Core.Entidades.Portal;
using Portal.Core.Entidades.GerenciamentoFiscal;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Portal.Core.Entidades.Universal
{
    public class ESTADO : ENTIDADE_DEFAULT
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string UF { get; set; }
        public string NOME { get; set; }
        public string CAPITAL { get; set; }
        public string GENTILICO { get; set; }

        [JsonIgnore]
        public virtual ICollection<ESTABELECIMENTO> ESTABELECIMENTOs { get; set; }
        [JsonIgnore]
        public virtual ICollection<PORTAL_USUARIO> PORTAL_USUARIOs { get; set; }
        [JsonIgnore]
        public virtual ICollection<FS_APURACAO_ICMSST> FS_APURACAO_ICMSSTs { get; set; }
    }
}
