﻿using Newtonsoft.Json;
using Portal.Core.Entidades.Comercial;
using Portal.Core.Entidades.Ecommerce;
using Portal.Core.Entidades.GerenciamentoFiscal;
using Portal.Core.Entidades.LojasVarejo;
using Portal.Core.Entidades.Universal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.Entidades.Universal
{
    public class ESTABELECIMENTO : ENTIDADE_DEFAULT
    {
        public ESTABELECIMENTO()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(12, ErrorMessage = "Campo obrigatório, (Max: {0})")]
        public string COD_ESTAB { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Campo obrigatório, (Max: {0}")]
        public string DESC_ESTAB { get; set; }

        public string CNPJ { get; set; }
        public string IE { get; set; }
        public string CEP { get; set; }
        public string ENDERECO { get; set; }
        public string BAIRRO { get; set; }
        public string COMPLEMENTO { get; set; }
        public string CIDADE { get; set; }

        public int? ESTADOID { get; set; }
        [ForeignKey("ESTADOID")]
        public virtual ESTADO ESTADO { get; set; }

        public string EMAIL { get; set; }
        public string CENTRO_CUSTO { get; set; }

        public int? REGIONALID { get; set; }
        [ForeignKey("REGIONALID")]
        public virtual CM_REGIONAL CM_REGIONAL { get; set; }

        public bool ATIVADO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        [JsonIgnore]
        public virtual ICollection<CM_GERENTE_LOJA> CM_GERENTE_LOJAs { get; set; }
        [JsonIgnore]
        public virtual ICollection<FS_CTRL_OBRIGACOES> FS_CTRL_OBRIGACOESs { get; set; }
        [JsonIgnore]
        public virtual ICollection<LJ_CONTROLSHOP_TOKEN> LJ_CONTROLSHOP_TOKEN { get; set; }
        [JsonIgnore]
        public virtual ICollection<LJ_CONTROLSHOP_CONTROLE> LJ_CONTROLSHOP_CONTROLEs { get; set; }
        [JsonIgnore]
        public virtual ICollection<EC_IMPRESSAO_NFE_CONTROLE> EC_IMPRESSAO_NFE_CONTROLEs { get; set; }
    }
}
