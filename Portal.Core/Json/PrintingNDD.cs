﻿using System.Collections.Generic;

namespace Portal.Core.Json
{
    public class Printer
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public string Server { get; set; }
            public string Path { get; set; }
            public string Port { get; set; }
            public List<int> UserIDs { get; set; }
            public int Limit { get; set; }
            public bool Active { get; set; }
        }

        public class PrintingNDD
        {
            public string Mode { get; set; }
            public List<Printer> Printers { get; set; }
        }

}
