﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.ViewModels.Portal
{
    public class vmMenu
    {
        public string UsuarioNome { get; set; }
        public string UsuarioEmail { get; set; }
        public int MenuID { get; set; }
        public string MenuNome { get; set; }
        public string MenuIcone { get; set; }
        public int MenuOrdem { get; set; }
        public string PaginaNome { get; set; }
        public string PaginaControllerName { get; set; }
        public string PaginaActionName { get; set; }
        public string PaginaIcone { get; set; }
        public int PaginaOrdem { get; set; }
    }
}
