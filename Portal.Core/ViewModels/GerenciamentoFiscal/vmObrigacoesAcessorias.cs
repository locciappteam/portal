﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Core.ViewModels.GerenciamentoFiscal
{
    public class vmObrigacoesAcessorias
    {
        public int ID { get; set; }
        public string UF { get; set; }
        public string COD_ESTAB { get; set; }
        public string DESC_ESTAB { get; set; }
        public DateTime DATA_APURACAO { get; set; }

        public string GIA { get; set; }
        public string GIA_ST{ get; set; }
        public string SPED_FISCAL { get; set; }
        public string SPED_CONTRIBUICOES { get; set; }
        public string SPED_REINF{ get; set; }
        public string DCTF { get; set; }
        public string DIRF { get; set; }
        public string NFP { get; set; }
        public string EDOC { get; set; }
        public string SEF { get; set; }
        public string DAC { get; set; }
        public string DIMI { get; set; }
        public string DCIP { get; set; }
        public string DMA { get; set; }
        public string ATO_COTEPE{ get; set; }
        public string DAPI { get; set; }
        
    }
}
