﻿var app = angular.module('appLoccitane', ['angularUtils.directives.dirPagination', 'angular-loading-bar', 'maskMoney', 'ng-bootstrap-select'])

app.factory('Estabelecimentos', function ($http) {
    var factory = {};

    factory.Obter = function () {
        return $http.get("/UserControl/ObterEstabelecimentos");
    }

    return factory;
});

app.filter("formataData", function () {
    var re = /\/Date\(([0-9]*)\)\//;
    return function (x) {
        var m = x.match(re);
        if (m) return new Date(parseInt(m[1]));
        else return null;
    };
});