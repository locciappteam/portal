﻿app.controller("ctrlImpressaoNFe", function ($scope, $filter, $http) {
    $scope.ListaImpressoesNFe = [];

    //Envia Formulário ============================================================================
    $scope.EnviaFormulario = function (isValid) {
        $http({
            method: "POST",
            url: "/ImpressaoNFe/Obter",
            dataType: "json",
            data: {
                UF: $scope.UF,
                Estabelecimento: $scope.Estabelecimento,
                DataInicial: $scope.DataInicial,
                DataFinal: $scope.DataFinal,

                ChaveNFe: $scope.ChaveNFe,
                NumeroNF: $scope.NumeroNF,
                SerieNF: $scope.SerieNF
            },
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(function (retorno) {
                $scope.ListaImpressoesNFe = retorno.data;
            })
            .catch(function (retorno) {
                toastr.error("Problemas ao obter informações de Impressão NF-e!", "Erro!");
                console.log(retorno);
            })
    }

    //Ordenar tabela ==============================================================================
    $scope.Ordenar = function (Coluna) {
        $scope.OrdenarColuna = Coluna;
        $scope.reverso = !$scope.reverso;
    }

    //Funções do Controller =======================================================================
    $scope.ImprimirNFe = function (isValid) {
        if (isValid) {
            $http({
                method: 'POST',
                url: '/ImpressaoNFe/ImprimirNFe',
                data: {
                    RefInterna: $scope.RefInterna
                }
            })
                .then(function (retorno) {
                    $scope.RefInterna = null;
                    if (retorno.data.Status == 0) {
                        toastr.warning(retorno.data.Mensagem, "Alerta!");
                    }
                    else {
                        toastr.success("Impressão enviada.", "Sucesso!");
                        $scope.ListaImpressoesNFe.unshift(retorno.data); //unshift adiciona na primeira posição
                    }
                })
                .catch(function (retorno) {
                    toastr.error("Erro ao imprimir.", "Erro!");
                    console.log(retorno);
                    $scope.RefInterna = null;
                });
        }
    }
});