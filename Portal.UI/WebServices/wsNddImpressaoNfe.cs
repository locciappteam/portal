﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml;

namespace Portal.UI.WebServices
{
    public class wsNddImpressaoNfe
    {
        public String InserirDocumento(string CHAVE_NFE, string LINHA_IMPRESSORA = "")
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"https://wscondc.e-datacenter.nddigital.com.br/eFormsWebServices/WSInserirDocumento.asmx");

            httpWebRequest.Accept = "text/xml";
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "text/xml;charset=utf-8";

            string strXML = string.Format(@"
                <soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:web='http://nddigital.com.br/eForms/webservices'>
                <soapenv:Header/>
                <soapenv:Body>
                    <web:eDocument>
                       <web:header><![CDATA[<eformsInserir xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns='http://www.nddigital.com.br/connector'><versao>1.00</versao><tipodocumento>1</tipodocumento><tiposervico>7</tiposervico><identificador>PD_NFE_LOCCITANE_ENTRY</identificador></eformsInserir>]]></web:header>
                       <web:document><![CDATA[<?xml version='1.0' encoding='utf-8'?><ImpressionRequest xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'><impressionitems><impressionitem><documentkey>{0}</documentkey><printer>{1}</printer></impressionitem></impressionitems></ImpressionRequest>]]></web:document>
                    </web:eDocument>
                </soapenv:Body>
                </soapenv:Envelope>
            ", CHAVE_NFE, LINHA_IMPRESSORA);

            XmlDocument SoapRequest = new XmlDocument();
            SoapRequest.LoadXml(strXML);

            string ServiceResponse;
            using (Stream stream = httpWebRequest.GetRequestStream())
            {
                SoapRequest.Save(stream);
            }
            using(WebResponse webResponse = httpWebRequest.GetResponse())
            {
                using (StreamReader streamReader = new StreamReader(webResponse.GetResponseStream()))
                {
                    ServiceResponse = streamReader.ReadToEnd();
                }
            }

            return ServiceResponse;
        }
    }
}