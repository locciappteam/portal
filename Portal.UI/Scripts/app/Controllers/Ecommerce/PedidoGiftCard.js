﻿app.controller("ctrlPedidoGiftcard", function ($scope, $filter, $http) {
    $scope.ListaPedidoGiftcard = [];

    //Envia Formulário ============================================================================
    $scope.EnviaFormulario = function (isValid) {
        $http({
            method: "POST",
            url: "/PedidoGiftcard/Obter",
            dataType: "json",
            data: {
                UF: $scope.UF,
                Estabelecimento: $scope.Estabelecimento,
                DataInicial: $scope.DataInicial,
                DataFinal: $scope.DataFinal,

                Pedido: $scope.filPedido,
                Giftcard: $scope.filGiftcard
            },
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(function (retorno) {
                $scope.ListaPedidoGiftcard = retorno.data;
            })
            .catch(function (retorno) {
                toastr.error("Problemas ao obter informações de Pedidos e Giftcard!", "ERRO!");
                console.log(retorno);
            })
    }

    //Ordenar tabela ==============================================================================
    $scope.Ordenar = function (Coluna) {
        $scope.OrdenarColuna = Coluna;
        $scope.reverso = !$scope.reverso;
    }

    //Adicionar Pedido e Giftcard =================================================================
    $scope.Salvar = function (isValid, EcPedidoGiftcard) {
        if (isValid) {
            $http({
                method: "POST",
                url: "/PedidoGiftcard/Salvar",
                dataType: "json",
                data: {
                    EcPedidoGiftcard: EcPedidoGiftcard
                },
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .then(function (retorno) {
                    if (retorno.data.msgRetorno != "Edicao")
                    {
                        toastr.success("Dados adicionados.", "Sucesso!");
                        $scope.ListaPedidoGiftcard.unshift(retorno.data); //unshift adiciona na primeira posição

                        $scope.EcPedidoGiftcard.PEDIDO = null;
                        $scope.EcPedidoGiftcard.GIFTCARD = null;

                        toastr.success("Registro Adicionado.", 'SUCESSO!');
                    }
                    else
                    {
                        toastr.success("Registro Alterado.", 'SUCESSO!');
                        $('#ModalDetalhar').modal('hide');
                    }
                })
                .catch(function (retorno) {
                    toastr.error("Erro ao adicionar/Alterar Pedido e Giftcard.", "ERRO!");
                    console.log(retorno);

                    $scope.EcPedidoGiftcard.PEDIDO = null;
                    $scope.EcPedidoGiftcard.GIFTCARD = null;
                });
        }
    }

    //Excluir Pedido e Giftcard ===================================================================
    $scope.Excluir = function (PedidoGiftcard, index) {
        if (confirm("Deseja excluir o registro abaixo?\n\n" +
            "Pedido: " + PedidoGiftcard.PEDIDO + "\n" +
            "Giftcard: " + PedidoGiftcard.GIFTCARD
        ))
        {
            $http({
                method: 'POST',
                url: '/PedidoGiftcard/Excluir',
                dataType: 'json',
                data: PedidoGiftcard,
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .then(function (retorno) {
                    switch (retorno.data.Retorno) {
                        case 1:
                            toastr.success(retorno.data.Mensagem, 'SUCESSO!');
                            $scope.ListaPedidoGiftcard.splice(index, 1);
                            break;
                        case 2:
                            toastr.error(retorno.data.Mensagem, 'ERRO!');
                    }
                })
                .catch(function (retorno) {
                    toastr.error('Erro ao excluir registro.', 'ERRO!');
                    console.log(retorno);
                });
        }
    }

    //Detalhar Pedido e Giftcard ==================================================================
    $scope.Detalhar = function (PedidoGiftcard) {
        $scope.Edicao = false;
        $scope.Descricao = "Detalhe do Documento";
        PedidoGiftcard.DATA_ALTERACAO = $filter('date')(PedidoGiftcard.DATA_ALTERACAO, 'dd/MM/yyyy HH:mm:ss');
        PedidoGiftcard.DATA_CADASTRO = $filter('date')(PedidoGiftcard.DATA_CADASTRO, 'dd/MM/yyyy HH:mm:ss');

        $scope.EcPedGift = PedidoGiftcard;
    }

    //Editar Pedido e Giftcard ====================================================================
    $scope.Editar = function (PedidoGiftcard) {
        $scope.Edicao = true;
        $scope.Descricao = "Edição do Documento";
        PedidoGiftcard.DATA_ALTERACAO = $filter('date')(PedidoGiftcard.DATA_ALTERACAO, 'dd/MM/yyyy HH:mm:ss');
        PedidoGiftcard.DATA_CADASTRO = $filter('date')(PedidoGiftcard.DATA_CADASTRO, 'dd/MM/yyyy HH:mm:ss');

        $scope.EcPedGift = PedidoGiftcard;

        $scope.EcPedGiftBKP = {};
        angular.copy($scope.EcPedGift, $scope.EcPedGiftBKP);
    }

    //Cancelar alteração Pedido e Giftcard ========================================================
    $scope.Cancelar = function () {
        if ($scope.Edicao == true) {
            angular.copy($scope.EcPedGiftBKP, $scope.EcPedGift);

            $scope.$apply;
        }
    }
});