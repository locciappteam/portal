﻿app.controller("ctrlControlShopXML", function ($scope, $filter, $http, Estabelecimentos) {
    //Get Estabelecimentos ========================================================================
    Estabelecimentos.Obter()
        .then(function (retorno) {
            $scope.ListaEstabelecimentos = retorno.data;
        })
        .catch(function (retorno) {
            alert("Erro ao carregar Estabelecimentos! " + retorno);
        });


    //Envia Formulário ============================================================================
    $scope.EnviaFormulario = function (isValid) {
        $http({
            method: "POST",
            url: "/ControlShopXML/Obter",
            dataType: "json",
            data: {
                UF: $scope.UF,
                Estabelecimento: $scope.Estabelecimento,
                DataInicial: $scope.DataInicial,
                DataFinal: $scope.DataFinal,
            },
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(function (retorno) {
                $scope.ListaControlShopXML = retorno.data;
            })
            .catch(function (retorno) {
                alert("Pelos poderes de Grayskull, deu erro!");
            })
    }

    //Ordenar tabela ==============================================================================
    $scope.Ordenar = function (Coluna) {
        $scope.OrdenarColuna = Coluna;
        $scope.reverso = !$scope.reverso;
    }

    //Funções do Controller =======================================================================
    $scope.Detalhar = function (ControlShopXML) {
        $scope.Edicao = false;

        ControlShopXML.DATA_INICIO = $filter('date')(ControlShopXML.DATA_INICIO, 'dd/MM/yyyy');
        ControlShopXML.DATA_FINAL = $filter('date')(ControlShopXML.DATA_FINAL, 'dd/MM/yyyy');
        $scope.ControlShopXml = ControlShopXML;

        $scope.Descricao = "Detalhe do Documento";
    }

    $scope.EnviarXml = function (isValid) {
        if (isValid) {
            $http({
                method: 'POST',
                url: '/ControlShopXML/EnviarXML',
                data: {
                    Estabelecimento: $scope.XmlEstabelecimentos,
                    DataInicial: $scope.XmlDataInicial,
                    DataFinal: $scope.XmlDataFinal,
                },
            })
                .then(function (retorno) {
                    switch (retorno.data.Status) {
                        case 1:
                            toastr.success(retorno.data.Mensagem, 'SUCESSO!');
                            $('#ModalEnviarXML').modal('hide');
                            break;
                        case 2:
                            toastr.error(retorno.data.Mensagem, 'ERRO!');
                            break;
                    }
                })
                .catch(function (retorno) {
                    toastr.error(retorno.data.Mensagem + ' : ' + retorno, 'ERRO!');

                    console.log(retorno);
                });
        }
    }
});

//app.factory('GetEstabelecimentos', function ($http) {
//    var factory{}
//});