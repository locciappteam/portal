﻿app.controller("ctrlObrigacoesAcessoarias", function ($scope, $filter, $http) {

    //Get Impostos ================================================================================
    $http({
        method: "GET",
        url: "/UserControl/ObterImpostos",
        dataType: "json"
    })
        .then(function (retorno) {
            $scope.Impostos = retorno.data
        })
        .catch(function (retorno) {
            alert("Erro ao carregar Lista de Impostos! " + retorno);
        });

    //Get Estados =================================================================================
    $http({
        method: "GET",
        url: "/UserControl/ObterEstados",
        dataType: "json"
    })
        .then(function (retorno) {
            $scope.Estados = retorno.data
        })
        .catch(function (retorno) {
            alert("Erro ao carregar Lista de Estados! " + retorno);
        });

    //Get Estabelecimentos ========================================================================
    $http({
        method: "GET",
        url: "/UserControl/ObterEstabelecimentos",
        dataType: "json"
    })
        .then(function (retorno) {
            $scope.Estabelecimentos = retorno.data
        })
        .catch(function (retorno) {
            alert("Erro ao carregar Lista de Estabelecimentos! " + retorno);
        });

    //Get Obrigações Fiscais ======================================================================
    $http({
        method: "GET",
        url: "/UserControl/ObterObrigacoesFiscais",
        dataType: "json"
    })
        .then(function (retorno) {
            $scope.ObrigacoesFiscais = retorno.data
        })
        .catch(function (retorno) {
            alert("Erro ao carregar Lista de Obrigações Fiscais! " + retorno);
        });

    //Get Status Obrigações Fiscais ===============================================================
    $scope.ObrigacoesStatus = [
        { 'DESCRICAO': 'ENTREGUE' },
        { 'DESCRICAO': 'ENTREGAR' },
        { 'DESCRICAO': 'RETIFICADO' },
        { 'DESCRICAO': 'RETIFICAR' }
    ];

    //Envia Formulário ============================================================================
    $scope.EnviaFormulario = function (isValid) {
        $http({
            method: "POST",
            url: "/ObrigacoesAcessorias/Obter",
            dataType: "json",
            data: {
                UF: $scope.UF,
                Estabelecimento: $scope.Estabelecimento,
                DataInicial: $scope.DataInicial,
                DataFinal: $scope.DataFinal,

                Obrigacoes: $scope.Obrigacoes,
                StatusObrigacao: $scope.StatusObrigacao
            },
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(function (retorno) {
                $scope.ListaObrigacoes = retorno.data;
            })
            .catch(function (retorno) {
                alert("Pelos poderes de Grayskull, deu erro!");
                console.log(retorno);
            })
    }

    //Ordenar tabela ==============================================================================
    $scope.Ordenar = function (Coluna) {
        $scope.OrdenarColuna = Coluna;
        $scope.reverso = !$scope.reverso;
    }

    //Funções do Controller =======================================================================
    $scope.Detalhar = function (obrigacao) {
        $scope.Edicao = false;
        $scope.Descricao = "Detalhe do Documento";
        obrigacao.DATA_APURACAO = $filter('date')(obrigacao.DATA_APURACAO, 'dd/MM/yyyy');

        $scope.Obrigacao = obrigacao;
    }
    $scope.Adicionar = function () {
        $scope.Edicao = true;
        $scope.Descricao = "Adicionar novo Documento"

        $scope.Obrigacao = {};
        $scope.Obrigacao.FS_APURACAO_ICMSSTs = [];
        $scope.Obrigacao.FS_APURACAO_RETIDOSs = [];
        $scope.Obrigacao.FS_OBRIGACAO_FISCALs = [];

        $scope.Obrigacao.DATA_CADASTRO = new Date();
    }
    $scope.Editar = function (obrigacao) {
        $scope.ICMSSTExcluidos = [];
        $scope.RetidosExcluidos = [];
        $scope.ObrigacoesFiscaisExcluidos = [];

        $scope.Edicao = true;
        $scope.Descricao = "Edição do Documento";
        obrigacao.DATA_APURACAO = $filter('date')(obrigacao.DATA_APURACAO, 'dd/MM/yyyy');

        $scope.Obrigacao = obrigacao;

        $scope.ObrigacaoBKP = {};
        angular.copy($scope.Obrigacao, $scope.ObrigacaoBKP);
    }

    $scope.Salvar = function (isValid) {
        $scope.Obrigacao.ESTABELECIMENTOID = $scope.Obrigacao.ESTABELECIMENTO.ID;

        if (isValid) {
            $http({
                method: 'POST',
                url: '/ObrigacoesAcessorias/Salvar',
                data: {
                    fs_ctrl_obrigacoes: $scope.Obrigacao,
                    ICMSSTExcluidos: $scope.ICMSSTExcluidos,
                    RetidosExcluidos: $scope.RetidosExcluidos,
                    ObrigacoesFiscaisExcluidos: $scope.ObrigacoesFiscaisExcluidos
                }
            })
                .then(function (retorno) {
                    switch (retorno.data.Retorno) {
                        case 1:
                        case 2:
                            toastr.success(retorno.data.Mensagem, 'SUCESSO!');
                            $('#ModalDetalhar').modal('hide');
                            break;
                        case 3:
                            toastr.error(retorno.data.Mensagem, 'ERRO!');
                            break;
                    }
                })
                .catch(function (retorno) {
                    toastr.error(retorno.data.Mensagem + ' : ' + retorno, 'ERRO!');

                    console.log(retorno);
                });
        }        
    }
    $scope.Excluir = function (obrigacao, index) {
        if (confirm("Deseja excluir a Obrigação Fiscal abaixo?\n\n" +
            "Estabelecimento: " + obrigacao.ESTABELECIMENTO.DESC_ESTAB + "\n" +
            "Data: " + $filter('date')(obrigacao.DATA_APURACAO, 'dd/MM/yyyy')
        )) {
            $http({
                method: 'POST',
                url: '/ObrigacoesAcessorias/Excluir',
                dataType: 'json',
                data: obrigacao,
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .then(function (retorno) {
                    switch (retorno.data.Retorno) {
                        case 1:
                            toastr.success(retorno.data.Mensagem, 'SUCESSO!');
                            $scope.ListaObrigacoes.splice(index, 1);
                            break;
                        case 2:
                            toastr.error(retorno.data.Mensagem, 'ERRO!');
                    }
                })
                .catch(function (retorno) {
                    toastr.error(retorno, 'ERRO!');

                    console.log(retorno);
                });
        }
    }
    $scope.Cancelar = function () {
        if ($scope.Edicao == true) {
            angular.copy($scope.ObrigacaoBKP, $scope.Obrigacao);
            $scope.$apply;
        }
    }

    //Funções ICMS-ST =============================================================================
    $scope.AdicionarICMSST = function () {
        $scope.Obrigacao.FS_APURACAO_ICMSSTs.push({
            ESTADO: $scope.Obrigacao.FS_APURACAO_ICMSSTs.ESTADO,
            ESTADOID: $scope.Obrigacao.FS_APURACAO_ICMSSTs.ESTADO.ID,
            SALDO_ANTERIOR: $scope.Obrigacao.FS_APURACAO_ICMSSTs.SALDO_ANTERIOR,
            CREDITO: $scope.Obrigacao.FS_APURACAO_ICMSSTs.CREDITO,
            DEBITO: $scope.Obrigacao.FS_APURACAO_ICMSSTs.DEBITO,
            LANCAMENTOS: $scope.Obrigacao.FS_APURACAO_ICMSSTs.LANCAMENTOS,
            VALOR_SALDO: $scope.Obrigacao.FS_APURACAO_ICMSSTs.VALOR_SALDO
        });

        $scope.Obrigacao.FS_APURACAO_ICMSSTs.ESTADO = "";
        $scope.Obrigacao.FS_APURACAO_ICMSSTs.SALDO_ANTERIOR = 0.00;
        $scope.Obrigacao.FS_APURACAO_ICMSSTs.CREDITO = 0.00;
        $scope.Obrigacao.FS_APURACAO_ICMSSTs.DEBITO = 0.00;
        $scope.Obrigacao.FS_APURACAO_ICMSSTs.LANCAMENTOS = 0.00;
        $scope.Obrigacao.FS_APURACAO_ICMSSTs.VALOR_SALDO = 0.00;
    }
    $scope.ExcluirICMSST = function (index) {
        if ($scope.Obrigacao.FS_APURACAO_ICMSSTs[index].CTRLOBRIGACOESID > 0) {
            $scope.ICMSSTExcluidos.push($scope.Obrigacao.FS_APURACAO_ICMSSTs[index]);
        }
        $scope.Obrigacao.FS_APURACAO_ICMSSTs.splice(index, 1);
    }

    //Funções Retido ==============================================================================
    $scope.AdicionarRetido = function () {
        $scope.Obrigacao.FS_APURACAO_RETIDOSs.push({
            FS_IMPOSTO: $scope.Obrigacao.FS_APURACAO_RETIDOSs.FS_IMPOSTO,
            IMPOSTOID: $scope.Obrigacao.FS_APURACAO_RETIDOSs.FS_IMPOSTO.ID,
            VALOR_IMPOSTO: $scope.Obrigacao.FS_APURACAO_RETIDOSs.VALOR_IMPOSTO            
        });

        $scope.Obrigacao.FS_APURACAO_RETIDOSs.FS_IMPOSTO = "";
        $scope.Obrigacao.FS_APURACAO_RETIDOSs.VALOR_IMPOSTO = 0.00;
    }
    $scope.ExcluirRetido = function (index) {
        if ($scope.Obrigacao.FS_APURACAO_RETIDOSs[index].CTRLOBRIGACOESID > 0) {
            $scope.RetidosExcluidos.push($scope.Obrigacao.FS_APURACAO_RETIDOSs[index]);
        }
        $scope.Obrigacao.FS_APURACAO_RETIDOSs.splice(index, 1);
    }

    //Funções Obrigação Fiscal ====================================================================
    $scope.AdicionarObrigacaoFiscal = function () {
        $scope.Obrigacao.FS_OBRIGACAO_FISCALs.push({
            FS_OBRIGACAO: $scope.Obrigacao.FS_OBRIGACAO_FISCALs.FS_OBRIGACAO,
            OBRIGACAOID: $scope.Obrigacao.FS_OBRIGACAO_FISCALs.FS_OBRIGACAO.ID,
            STATUS: $scope.Obrigacao.FS_OBRIGACAO_FISCALs.STATUS.DESCRICAO,
            OBSERVACAO: $scope.Obrigacao.FS_OBRIGACAO_FISCALs.OBSERVACAO
        });

        $scope.Obrigacao.FS_OBRIGACAO_FISCALs.FS_OBRIGACAO = "";
        $scope.Obrigacao.FS_OBRIGACAO_FISCALs.STATUS = "";
        $scope.Obrigacao.FS_OBRIGACAO_FISCALs.OBSERVACAO = "";
    }
    $scope.ExcluirObrigacaoFiscal = function (index) {
        if ($scope.Obrigacao.FS_OBRIGACAO_FISCALs[index].CTRLOBRIGACOESID > 0) {
            $scope.ObrigacoesFiscaisExcluidos.push($scope.Obrigacao.FS_OBRIGACAO_FISCALs[index]);
        }
        $scope.Obrigacao.FS_OBRIGACAO_FISCALs.splice(index, 1);
    }

    //Exportar ====================================================================================
    $scope.ExportarExcel = function () {
        alasql('SELECT * INTO XLSX("ObrigacoesFiscais.xlsx", {headers:true}) FROM ?', [$scope.ListaObrigacoes]);
    }
});