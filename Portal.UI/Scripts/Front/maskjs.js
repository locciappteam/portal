﻿$(document).ready(function () {
    $('.maskDecimal').mask("###0,00", { reverse: true });
    $('.maskTelefone').mask('(000) 0000-0000');
    $('.maskCelular').mask('(000) 00000-0000');
    $('.maskData').mask('00/00/0000');
    $('.maskCep').mask('00000-000');
    $('.maskCep').mask('00000-000');
    $('.maskIP').mask('99.999.999.999');
    $('.maskCNPJ').mask('00.000.000/0000-00', { reverse: true });
});