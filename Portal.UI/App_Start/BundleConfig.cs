﻿using System.Web;
using System.Web.Optimization;

namespace Portal.UI
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Scripts =============================================================================================
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/Jquery/jquery-{version}.js",
                "~/Scripts/Jquery/jquery.maskMoney.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/Jquery/jquery.validate*",
                "~/Scripts/Jquery/methods_pt.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryUI").Include(
                "~/Scripts/JqueryUI/jquery-*"
            ));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"
            ));

            bundles.Add(new ScriptBundle("~/bundles/BootstrapJS").Include(
                "~/Scripts/Bootstrap/bootstrap.js",
                "~/Scripts/Bootstrap/bootstrap-select.js",
                "~/Scripts/Bootstrap/Idiomas/defaults-pt_BR.min.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/AngularJS").Include(
                //AngularJS
                "~/Scripts/Angular/angular.min.js",
                "~/Scripts/i18n/angular-locale_pt-br.js",

                //Angular App e Controllers
                "~/Scripts/app/appLoccitane.js",

                "~/Scripts/app/Controllers/GerenciamentoFiscal/ObrigacoesAcessorias.js",
                "~/Scripts/app/Controllers/LojasVarejo/ControlShopXML.js",
                "~/Scripts/app/Controllers/Ecommerce/ImpressaoNFe.js",
                "~/Scripts/app/Controllers/Ecommerce/PedidoGiftCard.js",

                //Angular Modulos
                "~/Scripts/Angular/Modulos/ng-bootstrap-select.js",
                "~/Scripts/Angular/Modulos/angular-pagination.js",
                "~/Scripts/Angular/Modulos/loading-bar.min.js",
                "~/Scripts/Angular/Modulos/maskMoney.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/alaSQL").Include(
                "~/Scripts/alaSQL/alasql4.11.js",
                "~/Scripts/alaSQL/xlsx.full.min.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/JavaScripts").Include(
                "~/Scripts/Popper/umd/popper.min.js",
                "~/Scripts/Jquery/jquery.cookie.js",
                "~/Scripts/Front/jquery.mask.js",
                "~/Scripts/Front/front.js",
                "~/Scripts/Front/toastr.min.js",
                "~/Scripts/Front/LoccJS.js"
            ));
            //CSS =================================================================================================
            bundles.Add(new StyleBundle("~/Content/JqueryUICss").Include(
                "~/Content/JqueryUI/jquery-ui.css"
            ));

            bundles.Add(new StyleBundle("~/Content/BootstrapCss").Include(
                "~/Content/Bootstrap/bootstrap.css",
                "~/Content/Bootstrap/bootstrap-select.css"
            ));

            bundles.Add(new StyleBundle("~/Content/SiteFonts").Include(
                "~/fonts/font-awesome.min.css",
                "~/fonts/fontastic.css"
            ));

            bundles.Add(new StyleBundle("~/Content/SiteCss").Include(
                "~/Content/_Site/loading-bar.min.css",
                "~/Content/_Site/toastr.min.css",
                "~/Content/Site.css"
            ));
        }
    }
}
