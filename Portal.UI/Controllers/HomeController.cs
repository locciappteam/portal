﻿using Portal.Core.Repositorios.Portal;
using Portal.Core.ViewModels.Portal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Portal.UI.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        readonly irptPortalCtrlAcesso _rptPortalCtrlAcesso = new rptPortalCtrlAcesso();
        private readonly irptPortalUsuario _rptPortalUsuario = new rptPortalUsuario();

        public ActionResult Index()
        {
            var Usuario = _rptPortalUsuario.Obter().Where(a => a.LOGIN == User.Identity.Name.ToLower()).First();
            ViewBag.Usuario = Usuario.NOME;
            ViewBag.Departamento = Usuario.PORTAL_DEPARTAMENTO.NOME;

            return View();
        }

        [ChildActionOnly]
        public PartialViewResult Menu()
        {
            var MenuPersonalizado = _rptPortalCtrlAcesso.Obter()
            .Where(a =>
                a.PORTAL_PAGINA.PORTAL_MENU.ATIVADO == true
                && a.PORTAL_PAGINA.ATIVADO == true
                && a.PORTAL_USUARIO.LOGIN.ToLower() == User.Identity.Name.ToLower()
            )
            .Select(a => new vmMenu
            {
                UsuarioNome = a.PORTAL_USUARIO.NOME,
                UsuarioEmail = a.PORTAL_USUARIO.EMAIL,

                MenuID = a.PORTAL_PAGINA.PORTAL_MENU.ID,
                MenuNome = a.PORTAL_PAGINA.PORTAL_MENU.NOME,
                MenuIcone = a.PORTAL_PAGINA.PORTAL_MENU.ICONE,
                MenuOrdem = a.PORTAL_PAGINA.PORTAL_MENU.ORDENAR,

                PaginaNome = a.PORTAL_PAGINA.NOME,
                PaginaControllerName = a.PORTAL_PAGINA.CONTROLLERNAME,
                PaginaActionName = a.PORTAL_PAGINA.ACTIONNAME,
                PaginaIcone = a.PORTAL_PAGINA.ICONE,
                PaginaOrdem = a.PORTAL_PAGINA.ORDENAR
            }).Distinct().ToList();

            return PartialView("~/Views/Shared/_Menu.cshtml", MenuPersonalizado);
        }        
    }
}