﻿using Newtonsoft.Json;
using Portal.Core.Data;
using Portal.Core.Entidades.GerenciamentoFiscal;
using Portal.Core.Parametros.GerenciamentoFiscal;
using Portal.Core.Repositorios.GerenciamentoFiscal;
using Portal.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace Portal.UI.Controllers.GerenciamentoFiscal
{
    [Authorize]
    public class ObrigacoesAcessoriasController : Controller
    {
        private readonly irptCtrlObrigacoes _rptCtrlObrigacoes = new rptCtrlObrigacoes();
        private readonly irptICMS _rptICMS = new rptICMS();
        private readonly irptICMSST _rptICMSST = new rptICMSST();
        private readonly irptIPI _rptIPI = new rptIPI();
        private readonly irptPIS _rptPIS = new rptPIS();
        private readonly irptCofins _rptCofins = new rptCofins();
        private readonly irptISS _rptISS = new rptISS();
        private readonly irptRetidos _rptRetidos = new rptRetidos();
        private readonly irptObrigacaoFiscal _rptObrigacaoFiscal = new rptObrigacaoFiscal();

        private readonly irptObrigacao _rptObrigacao = new rptObrigacao();

        // GET: ObrigacoesAcessorias
        [PermissaoFiltro(Roles = "ObrigacoesAcessorias")]
        public ActionResult Index()
        {
            ViewBag.Obrigacao = _rptObrigacao.Obter();

            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        [PermissaoFiltro(Roles = "ObrigacoesAcessorias")]
        public String Obter(pObrigacoesAcessorias p)
        {
            List<FS_CTRL_OBRIGACOES> Obrigacoes = _rptCtrlObrigacoes.Obter()
                .Where(a =>
                    (a.DATA_APURACAO >= p.DataInicial && a.DATA_APURACAO <= p.DataFinal) 
                    && (p.UF.Contains(a.ESTABELECIMENTO.ESTADO.ID) || !p.UF.Any()) 
                    && (p.Estabelecimento.Contains(a.ESTABELECIMENTOID) || !p.Estabelecimento.Any())
                    && (a.FS_OBRIGACAO_FISCALs.Any(b => p.Obrigacoes.Contains(b.OBRIGACAOID)) || !p.Obrigacoes.Any())
                    &&(a.FS_OBRIGACAO_FISCALs.Any(b => p.StatusObrigacao.Contains(b.STATUS)) || !p.StatusObrigacao.Any())
                ).ToList();

            //var json = JsonConvert.SerializeObject(Obrigacoes, Formatting.None);

            return JsonConvert.SerializeObject(Obrigacoes, Formatting.None);
        }

        [HttpPost]
        [PermissaoFiltro(Roles = "ObrigacoesAcessorias")]
        public JsonResult Salvar(FS_CTRL_OBRIGACOES fs_ctrl_obrigacoes, 
                                 List<FS_APURACAO_ICMSST> ICMSSTExcluidos, 
                                 List<FS_APURACAO_RETIDOS> RetidosExcluidos, 
                                 List<FS_OBRIGACAO_FISCAL> ObrigacoesFiscaisExcluidos)
        {
            if (ModelState.IsValid)
            {
                fs_ctrl_obrigacoes.ESTABELECIMENTO = null;
                foreach (var icmsst in fs_ctrl_obrigacoes.FS_APURACAO_ICMSSTs ?? Enumerable.Empty<FS_APURACAO_ICMSST>())
                {
                    icmsst.ESTADO = null;
                }
                foreach (var retido in fs_ctrl_obrigacoes.FS_APURACAO_RETIDOSs ?? Enumerable.Empty<FS_APURACAO_RETIDOS>())
                {
                    retido.FS_IMPOSTO = null;
                }
                foreach (var obrigacaofiscal in fs_ctrl_obrigacoes.FS_OBRIGACAO_FISCALs ?? Enumerable.Empty<FS_OBRIGACAO_FISCAL>())
                {
                    obrigacaofiscal.FS_OBRIGACAO = null;
                }

                if (fs_ctrl_obrigacoes.ID == 0)
                {
                    fs_ctrl_obrigacoes.CADASTRADO_POR = User.Identity.Name.ToLower();
                    fs_ctrl_obrigacoes.ALTERADO_POR = User.Identity.Name.ToLower();

                    _rptCtrlObrigacoes.Adicionar(fs_ctrl_obrigacoes);

                    return Json(new { Mensagem = "Cadastro salvo!", Retorno = 1 });
                }
                else
                {
                    _rptICMS.Editar(fs_ctrl_obrigacoes.FS_APURACAO_ICMS);
 
                    foreach (var icmsst in fs_ctrl_obrigacoes.FS_APURACAO_ICMSSTs ?? Enumerable.Empty<FS_APURACAO_ICMSST>())
                    {
                        if(icmsst.ID == 0)
                        { 
                            icmsst.CTRLOBRIGACOESID = fs_ctrl_obrigacoes.ID;
                            _rptICMSST.Adicionar(icmsst);
                        }
                        else
                        {
                            _rptICMSST.Editar(icmsst);
                        }                            
                    }

                    _rptIPI.Editar(fs_ctrl_obrigacoes.FS_APURACAO_IPI);
                    _rptPIS.Editar(fs_ctrl_obrigacoes.FS_APURACAO_PIS);
                    _rptCofins.Editar(fs_ctrl_obrigacoes.FS_APURACAO_COFINS);
                    _rptISS.Editar(fs_ctrl_obrigacoes.FS_APURACAO_ISS);

                    foreach (var retido in fs_ctrl_obrigacoes.FS_APURACAO_RETIDOSs ?? Enumerable.Empty<FS_APURACAO_RETIDOS>())
                    {
                        if (retido.ID == 0)
                        {
                            retido.CTRLOBRIGACOESID = fs_ctrl_obrigacoes.ID;
                            _rptRetidos.Adicionar(retido);
                        }
                        else
                        {
                            _rptRetidos.Editar(retido);
                        }
                    }

                    foreach (var obrigacaofiscal in fs_ctrl_obrigacoes.FS_OBRIGACAO_FISCALs ?? Enumerable.Empty<FS_OBRIGACAO_FISCAL>())
                    {
                        if (obrigacaofiscal.ID == 0)
                        {
                            obrigacaofiscal.CTRLOBRIGACOESID = fs_ctrl_obrigacoes.ID;
                            _rptObrigacaoFiscal.Adicionar(obrigacaofiscal);
                        }
                        else
                        {
                            _rptObrigacaoFiscal.Editar(obrigacaofiscal);
                        }
                    }
                    _rptCtrlObrigacoes.Editar(fs_ctrl_obrigacoes);

                    return Json(new { Mensagem = "Alteração salva!", Retorno = 2 });
                }
            }

            foreach (var icmsstexcluido in ICMSSTExcluidos ?? Enumerable.Empty<FS_APURACAO_ICMSST>())
            {
                _rptICMSST.Excluir(icmsstexcluido);
            }

            foreach (var retidoexcluido in RetidosExcluidos ?? Enumerable.Empty<FS_APURACAO_RETIDOS>())
            {
                _rptRetidos.Excluir(retidoexcluido);
            }

            foreach (var obrigacaofiscalexcluido in ObrigacoesFiscaisExcluidos ?? Enumerable.Empty<FS_OBRIGACAO_FISCAL>())
            {
                _rptObrigacaoFiscal.Excluir(obrigacaofiscalexcluido);
            }

            return Json(new { Mensagem = "Problemas ao salvar!", Retorno = 3 });
        }

        [HttpPost]
        [PermissaoFiltro(Roles = "ObrigacoesAcessorias")]
        public JsonResult Excluir(FS_CTRL_OBRIGACOES fs_ctrl_obrigacoes)
        {
            if(ModelState.IsValid)
            {
                _rptCtrlObrigacoes.Excluir(fs_ctrl_obrigacoes);

                return Json(new { Mensagem = "Cadastro Excluído!", Retorno = 1 });
            }

            return Json(new { Mensagem = "Erro ao Excluir!", Retorno = 2 });
        }

        protected override void Dispose(bool disposing)
        {
            _rptCtrlObrigacoes.Dispose();
            _rptICMS.Dispose();
            _rptICMSST.Dispose();
            _rptIPI.Dispose();
            _rptPIS.Dispose();
            _rptCofins.Dispose();
            _rptISS.Dispose();
            _rptRetidos.Dispose();
            _rptObrigacaoFiscal.Dispose();

            _rptObrigacao.Dispose();
        }
    }
}