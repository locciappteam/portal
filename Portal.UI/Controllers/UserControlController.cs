﻿using Newtonsoft.Json;
using Portal.Core.Entidades.Universal;
using Portal.Core.Parametros.UserControl;
using Portal.Core.Repositorios.GerenciamentoFiscal;
using Portal.Core.Repositorios.Universal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Portal.UI.Controllers
{
    public class UserControlController : Controller
    {
        private readonly irptEstado _rptEstado = new rptEstado();
        private readonly irptEstabelecimento _rptEstabelecimento = new rptEstabelecimento();
        private readonly irptImposto _rptImposto = new rptImposto();
        private readonly irptObrigacao _rptObrigacao = new rptObrigacao();
            
        // GET: UserControl
        public ActionResult Index()
        {
            return View();
        }

        [ChildActionOnly]
        public PartialViewResult DropDownListEstado(pUserControlBase parametros)
        {
            ViewBag.Estado = _rptEstado.Obter();

            return PartialView("~/Views/_UserControl/_Estado.cshtml", parametros);
        }

        [ChildActionOnly]
        public PartialViewResult DropDownListEstabelecimento(pUserControlBase parametros)
        {
            ViewBag.Estabelecimento = _rptEstabelecimento.Obter();

            return PartialView("~/Views/_UserControl/_Estabelecimento.cshtml", parametros);
        }

        public String ObterImpostos ()
        {
            return JsonConvert.SerializeObject(_rptImposto.Obter().Where(a=> new[] { 6, 7, 8, 9, 11 }.Contains(a.ID)), Formatting.None);
        }

        public String ObterEstados()
        {
            return JsonConvert.SerializeObject(_rptEstado.Obter(), Formatting.None);
        }

        public String ObterObrigacoesFiscais()
        {
            return JsonConvert.SerializeObject(_rptObrigacao.Obter(), Formatting.None);
        }

        public String ObterEstabelecimentos()
        {
            return JsonConvert.SerializeObject(_rptEstabelecimento.Obter(), Formatting.None);
        }

        protected override void Dispose(bool disposing)
        {
            _rptEstado.Dispose();
            _rptEstabelecimento.Dispose();
        }
    }
}