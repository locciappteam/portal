﻿using Newtonsoft.Json;
using Portal.Core.Data;
using Portal.Core.Entidades.Ecommerce;
using Portal.Core.Entidades.FilaImpressao;
using Portal.Core.Json;
using Portal.Core.Parametros.Ecommerce;
using Portal.Core.Repositorios.Comercial;
using Portal.Core.Repositorios.Ecommerce;
using Portal.Core.Repositorios.FilaImpressao;
using Portal.Core.Repositorios.Portal;
using Portal.Core.Repositorios.Universal;
using Portal.Core.XML;
using Portal.UI.Models;
using Portal.UI.WebServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace Portal.UI.Controllers.Ecommerce
{
    [Authorize]
    public class ImpressaoNFeController : Controller
    {
        private readonly ViewsDataContext _views = new ViewsDataContext();
        private readonly irptImpressaoNFeCtrl _rptImpressaoNFeCtrl = new rptImpressaoNFeCtrl();
        private readonly irptEstabelecimento _rptEstabelecimento = new rptEstabelecimento();

        // GET: ImpressaoNFe
        [PermissaoFiltro(Roles = "ImpressaoNFe")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [PermissaoFiltro(Roles = "ImpressaoNFe")]
        public String Obter(pImpressaoNFe p)
        {
            List<EC_IMPRESSAO_NFE_CONTROLE> ImpressoesNFe = _rptImpressaoNFeCtrl.Obter()
                .Where(a =>
                    (a.DATA_CADASTRO.Date >= p.DataInicial && a.DATA_CADASTRO.Date <= p.DataFinal)
                    && (p.UF.Contains(a.ESTABELECIMENTO.ESTADO.ID) || !p.UF.Any())
                    && (p.Estabelecimento.Contains(a.ESTABELECIMENTOID) || !p.Estabelecimento.Any())
                    && (a.CHAVE_NFE == p.ChaveNFe || p.ChaveNFe == null)
                    && (a.NUMERO_NF == p.NumeroNF || p.NumeroNF == null)
                    && (a.SERIE_NF == p.SerieNF || p.SerieNF == null)
                ).ToList();

            return JsonConvert.SerializeObject(ImpressoesNFe, Formatting.None);
        }

        [HttpPost]
        [PermissaoFiltro(Roles = "ImpressaoNFe")]
        public String ImprimirNFe(string RefInterna)
        {

                string _printer = null;
                int? _idPrinter = null;

                var user = new rptPortalUsuario().Obter().Where(p => p.LOGIN == User.Identity.Name).Select(p => p.ID).FirstOrDefault();
                var impressoras = JsonConvert.DeserializeObject<PrintingNDD>(new rptParametro().Obter().Where(p => p.PARAMETRO == "PRINTING_NDD").Select(p => p.VALOR).FirstOrDefault());

                _printer = impressoras.Printers.Where(p => p.UserIDs.Contains(user)).Select(p => p.Name).FirstOrDefault();
                _idPrinter = impressoras.Printers.Where(p => p.UserIDs.Contains(user)).Select(p => p.ID).FirstOrDefault();

                if (_printer == null)
                {
                    return JsonConvert.SerializeObject(new { Status = 0, Mensagem = "Não é possível imprimir. Nenhuma impressora ativa para esse usuário!" }, Formatting.None);
                }

            W_IMPRESSAO_NFE ImpressaoNFe = (
                    from
                        a in _views.W_IMPRESSAO_NFEs
                    where
                        a.REF_INTERNA.Replace("BR0001-", "") == RefInterna.Replace("BR0001-", "")
                    orderby
                        a.DATA_EMISSAO descending
                    select
                        a
                ).FirstOrDefault();


            if (ImpressaoNFe != null)
            {
                irptControleImpressao irptControleImpressao = new rptControleImpressao();

                FI_CONTROLE_IMPRESSAO retornoImpressao = new rptControleImpressao().Adicionar(
                new FI_CONTROLE_IMPRESSAO
                {
                    DATA_SOLICITACAO = DateTime.Now,
                    REF_INTERNA = RefInterna,
                    IDIMPRESSORA = (byte?)_idPrinter,
                    STATUS = 1
                });

                try
                {
                    wsNddImpressaoNfe ws = new wsNddImpressaoNfe();
                    var ServiceResponse = ws.InserirDocumento(ImpressaoNFe.CHAVE_NFE, _printer);

                    ImpressaoNFeEnvelope Response = new ImpressaoNFeEnvelope();
                    using (TextReader textReader = new StringReader(ServiceResponse))
                    {
                        var serializer = new XmlSerializer(typeof(ImpressaoNFeEnvelope));
                        Response = (ImpressaoNFeEnvelope)serializer.Deserialize(textReader);
                    }

                    EC_IMPRESSAO_NFE_CONTROLE _EcImpressaoNFeControle = new EC_IMPRESSAO_NFE_CONTROLE
                    {
                        ESTABELECIMENTOID = ImpressaoNFe.ESTABELECIMENTOID,
                        PEDIDO = RefInterna,
                        CHAVE_NFE = ImpressaoNFe.CHAVE_NFE,
                        NUMERO_NF = ImpressaoNFe.NUMERO_NF,
                        SERIE_NF = ImpressaoNFe.SERIE_NF,
                        DATA_EMISSAO = ImpressaoNFe.DATA_EMISSAO,
                        CADASTRADO_POR = User.Identity.Name.ToLower(),
                        DATA_CADASTRO = DateTime.Now,
                        ALTERADO_POR = User.Identity.Name.ToLower(),
                        DATA_ALTERACAO = DateTime.Now,
                        EC_IMPRESSAO_NFE_RETORNOs = new List<EC_IMPRESSAO_NFE_RETORNO>()
                    };

                    EC_IMPRESSAO_NFE_RETORNO _EcImpressaoNFeRetorno = new EC_IMPRESSAO_NFE_RETORNO
                    {
                        CADASTRADO_POR = User.Identity.Name.ToLower(),
                        DATA_CADASTRO = DateTime.Now,
                        ALTERADO_POR = User.Identity.Name.ToLower(),
                        DATA_ALTERACAO = DateTime.Now
                    };

                    _EcImpressaoNFeRetorno.STATUS_RETORNO = "Impressão realizada com sucesso.";
                    _EcImpressaoNFeControle.EC_IMPRESSAO_NFE_RETORNOs.Add(_EcImpressaoNFeRetorno);
                    _rptImpressaoNFeCtrl.Adicionar(_EcImpressaoNFeControle);
                    _EcImpressaoNFeControle.ESTABELECIMENTO = _rptEstabelecimento.ObterPorID(_EcImpressaoNFeControle.ESTABELECIMENTOID);

                    retornoImpressao.DATA_IMPRESSAO = DateTime.Now;
                    retornoImpressao.STATUS = 3;
                    var retornoImpressaoOK = new rptControleImpressao().Editar(retornoImpressao);

                    return JsonConvert.SerializeObject(_EcImpressaoNFeControle, Formatting.None);

                }
                catch (Exception ex)
                {
                    retornoImpressao.STATUS = 2;
                    var retornoImpressaoErr = new rptControleImpressao().Editar(retornoImpressao);
                    return JsonConvert.SerializeObject(new { Status = 0, Mensagem = "Erro ao Imprimir!: " + ex.Message }, Formatting.None);
                }
            }
            else
            {
                return JsonConvert.SerializeObject(new { Status = 0, Mensagem = "Pedido não encontrado!" }, Formatting.None);
            }
        }


        protected override void Dispose(bool disposing)
        {
            _views.Dispose();
            _rptImpressaoNFeCtrl.Dispose();
            _rptEstabelecimento.Dispose();
        }
    }
}