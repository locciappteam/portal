﻿using Newtonsoft.Json;
using Portal.Core.Data;
using Portal.Core.Entidades.Ecommerce;
using Portal.Core.Parametros.Ecommerce;
using Portal.Core.Repositorios.Ecommerce;
using Portal.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Portal.UI.Controllers.Ecommerce
{
    [Authorize]
    public class PedidoGiftcardController : Controller
    {
        private readonly PortalDataContext _ctx = new PortalDataContext();
        private readonly irptPedidoGiftcard _rptPedidoGiftcard = new rptPedidoGiftcard();

        // GET: PedidoGiftcard
        [PermissaoFiltro(Roles = "PedidoGiftcard")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [PermissaoFiltro(Roles = "PedidoGiftcard")]
        public String Obter(pPedidoGiftcard p)
        {
            p.DataFinal = p.DataFinal.AddDays(1);
            List<EC_PEDIDO_GIFTCARD> PedidoGiftCard = (
                from
                    a in _ctx.EcPedidoGiftCard
                where
                    (a.DATA_CADASTRO >= p.DataInicial && a.DATA_CADASTRO < p.DataFinal)
                    && (a.GIFTCARD.Contains(p.Giftcard) || p.Giftcard == null)
                    && (a.PEDIDO.Contains(p.Pedido) || p.Pedido == null)
                select
                    a
            ).ToList();

            return JsonConvert.SerializeObject(PedidoGiftCard, Formatting.None);
        }

        [HttpPost]
        [PermissaoFiltro(Roles = "PedidoGiftcard")]
        public String Salvar(EC_PEDIDO_GIFTCARD EcPedidoGiftcard)
        {
            if (EcPedidoGiftcard.ID == 0)
            {
                EcPedidoGiftcard.CADASTRADO_POR = User.Identity.Name.ToLower();
                EcPedidoGiftcard.DATA_CADASTRO = DateTime.Now;
                EcPedidoGiftcard.ALTERADO_POR = User.Identity.Name.ToLower();

                _rptPedidoGiftcard.Adicionar(EcPedidoGiftcard);

                return JsonConvert.SerializeObject(EcPedidoGiftcard, Formatting.None);
            }
            else
            {
                EcPedidoGiftcard.CADASTRADO_POR = User.Identity.Name.ToLower();
                EcPedidoGiftcard.ALTERADO_POR = User.Identity.Name.ToLower();

                _rptPedidoGiftcard.Editar(EcPedidoGiftcard);

                return @"{ ""msgRetorno"" : ""Edicao"" }";
            }
        }

        [HttpPost]
        [PermissaoFiltro(Roles = "PedidoGiftcard")]
        public JsonResult Excluir (EC_PEDIDO_GIFTCARD EcPedidoGiftcard)
        {
            if (ModelState.IsValid)
            {
                _rptPedidoGiftcard.Excluir(EcPedidoGiftcard);

                return Json(new { Mensagem = "Registro Excluído!", Retorno = 1 });
            }

            return Json(new { Mensagem = "Erro ao Excluir!", Retorno = 2 });
        }

        protected override void Dispose(bool disposing)
        {
            _ctx.Dispose();
            _rptPedidoGiftcard.Dispose();
        }
    }
}