﻿using Portal.Core.Entidades.Portal;
using Portal.Core.Repositorios.Portal;
using Portal.Core.ViewModels.Portal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Portal.UI.Controllers
{
    [Authorize]
    public class LoginController : Controller
    {
        readonly irptPortalUsuario _rptPortalUsuario = new rptPortalUsuario();

        // GET: Login
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(vmLogin Login)
        {
            if (ModelState.IsValid)
            {
                PORTAL_USUARIO Autenticado = _rptPortalUsuario.Autenticar(Login.Usuario, Login.Senha);
                if (Autenticado == null)
                {
                    ModelState.AddModelError("", "Usuário ou senha inválida!");
                    return View(Login);
                }
                //FormsAuthentication.SetAuthCookie(Autenticado.LOGIN, false);
                FormsAuthenticationTicket Ticket = new FormsAuthenticationTicket(
                    1,
                    Login.Usuario,
                    DateTime.Now,
                    DateTime.Now.AddMinutes(60),
                    true,
                    "",
                    FormsAuthentication.FormsCookiePath
                );
                Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(Ticket)));

                return RedirectToAction("Index", "Home");
            }

            return View(Login);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

        public ActionResult AcessoNegado()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            _rptPortalUsuario.Dispose();
        }
    }
}