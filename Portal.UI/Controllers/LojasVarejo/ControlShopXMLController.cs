﻿using Newtonsoft.Json;
using Portal.Core;
using Portal.Core.Data;
using Portal.Core.Entidades;
using Portal.Core.Entidades.LojasVarejo;
using Portal.Core.Parametros.LojasVarejo;
using Portal.Core.Repositorios.LojasVarejo;
using Portal.UI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace Portal.UI.Controllers.LojasVarejo
{
    [Authorize]
    public class ControlShopXMLController : Controller
    {
        private readonly irptControlShopCtrl _rptControlShopCtrl = new rptControlShopCtrl();
        private readonly ControlShop _ControlShop = new ControlShop();

        // GET: ControlShopXML
        [PermissaoFiltro(Roles = "ControlShopXML")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [PermissaoFiltro(Roles = "ControlShopXML")]
        public String Obter(pControlShopXML p)
        {
            List<LJ_CONTROLSHOP_CONTROLE> ControlShopCtrl = _rptControlShopCtrl.Obter()
                .Where(a =>
                    (p.UF.Contains(a.ESTABELECIMENTO.ESTADO.ID) || !p.UF.Any())
                    && (p.Estabelecimento.Contains(a.ESTABELECIMENTOID) || !p.Estabelecimento.Any())
                    && (a.DATA_INICIO >= p.DataInicial && a.DATA_FINAL <= p.DataFinal)
                ).ToList();

            return JsonConvert.SerializeObject(ControlShopCtrl, Newtonsoft.Json.Formatting.None);
        }

        [HttpPost]
        [PermissaoFiltro(Roles = "ControlShopXML")]
        public JsonResult EnviarXML(pControlShopXML p)
        {
            try
            {
                foreach (var EstabelecimentoId in p.Estabelecimento)
                {
                    string CaminhoXML = Server.MapPath("~/Uploads/ControlShop/XMLs/");
                    if (!Directory.Exists(CaminhoXML))
                    {
                        Directory.CreateDirectory(CaminhoXML);
                    }
                    else
                    {
                        Directory.Delete(CaminhoXML, true);
                        Directory.CreateDirectory(CaminhoXML);
                    }

                    ICollection<W_CONTROLSHOP_XML> XMLs = _ControlShop.ObterXML(EstabelecimentoId, p.DataInicial, p.DataFinal);

                    if (XMLs.Any())
                    {
                        _ControlShop.GerarXML(XMLs, CaminhoXML);

                        string ArquivoZip = _ControlShop.ZiparXML(XMLs, CaminhoXML);

                        byte[] ZipByte = System.IO.File.ReadAllBytes(ArquivoZip);
                        string TokenWebService = XMLs.Select(a => a.TOKEN_WEBSERVICE).FirstOrDefault();
                        string NomeDoArquivo = Path.GetFileName(ArquivoZip);

                        var ws = new wsControlShop.CorporateSoapClient();                               //Proxy no Web.config
                        string Retorno = ws.RecebePacoteNotas(TokenWebService, NomeDoArquivo, ZipByte); //Proxy no Web.config

                        LJ_CONTROLSHOP_CONTROLE _LjControleShopControle = new LJ_CONTROLSHOP_CONTROLE
                        {
                            ESTABELECIMENTOID = EstabelecimentoId,
                            DATA_INICIO = p.DataInicial,
                            DATA_FINAL = p.DataFinal,
                            QTDE_XML = XMLs.Count(),
                            CADASTRADO_POR = User.Identity.Name.ToLower(),
                            DATA_CADASTRO = DateTime.Now,
                            ALTERADO_POR = User.Identity.Name.ToLower(),
                            DATA_ALTERACAO = DateTime.Now,
                            LJ_CONTROLSHOP_RETORNOs = new List<LJ_CONTROLSHOP_RETORNO>()
                        };
                        LJ_CONTROLSHOP_RETORNO _LjControlShopRetorno = new LJ_CONTROLSHOP_RETORNO
                        {
                            CADASTRADO_POR = User.Identity.Name.ToLower(),
                            DATA_CADASTRO = DateTime.Now,
                            ALTERADO_POR = User.Identity.Name.ToLower(),
                            DATA_ALTERACAO = DateTime.Now
                        };

                        if (Retorno == "Aquivo recebido e descompactado com sucesso !")
                        {
                            if (!System.IO.File.Exists(Server.MapPath("~/Uploads/ControlShop/Packages/") + Path.GetFileName(ArquivoZip)))
                            {
                                System.IO.File.Move(ArquivoZip, Server.MapPath("~/Uploads/ControlShop/Packages/") + Path.GetFileName(ArquivoZip));
                            }
                            else
                            {
                                System.IO.File.Delete(Server.MapPath("~/Uploads/ControlShop/Packages/") + Path.GetFileName(ArquivoZip));
                                System.IO.File.Move(ArquivoZip, Server.MapPath("~/Uploads/ControlShop/Packages/") + Path.GetFileName(ArquivoZip));
                            }

                            _LjControleShopControle.ARQUIVO = "/Uploads/ControlShop/Packages/" + Path.GetFileName(ArquivoZip);
                            _LjControlShopRetorno.STATUS_RETORNO = "Arquivo recebido e descompactado com sucesso !";
                            _LjControleShopControle.LJ_CONTROLSHOP_RETORNOs.Add(_LjControlShopRetorno);

                            _rptControlShopCtrl.Adicionar(_LjControleShopControle);
                        }
                        else
                        {
                            if (!System.IO.File.Exists(Server.MapPath("~/Uploads/ControlShop/Error/") + Path.GetFileName(ArquivoZip)))
                            {
                                System.IO.File.Move(ArquivoZip, Server.MapPath("~/Uploads/ControlShop/Error/") + Path.GetFileName(ArquivoZip));
                            }
                            else
                            {
                                System.IO.File.Delete(Server.MapPath("~/Uploads/ControlShop/Error/") + Path.GetFileName(ArquivoZip));
                                System.IO.File.Move(ArquivoZip, Server.MapPath("~/Uploads/ControlShop/Error/") + Path.GetFileName(ArquivoZip));
                            }

                            _LjControleShopControle.ARQUIVO = "/Uploads/ControlShop/Error/" + Path.GetFileName(ArquivoZip);
                            _LjControlShopRetorno.STATUS_RETORNO = Retorno;
                            _LjControleShopControle.LJ_CONTROLSHOP_RETORNOs.Add(_LjControlShopRetorno);

                            _rptControlShopCtrl.Adicionar(_LjControleShopControle);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : ========== ERRO FATAL ========== \n {ex.ToString()}"));
            }

            return Json(new { Mensagem = "Arquivos enviados!", Status = 1 });
        }

        protected override void Dispose(bool disposing)
        {
            _rptControlShopCtrl.Dispose();
        }
    }
}